# run using :
# $> python manage.py shell < dbseeder.py

import json
from engine.models import *
from bots.models import *

with open("utils/fixtures/seed.json", "r", encoding="utf-8") as fi:
    print("Reading seed file...")
    seed_object = json.loads(fi.read())

b, _ = Bot.objects.get_or_create(title="Small bot")
modules = []
for mod, ints in seed_object.items():
    _m, _ = Module.objects.get_or_create(title=mod)
    modules.append(_m)
    for intention, samples in ints.items():
        _i, _ = Intention.objects.get_or_create(title=intention, module=_m)
        
        if samples.get("variables", None):
            for v in samples["variables"]:
                _s, _ = Variable.objects.get_or_create(name=v, intention=_i)
                _bv = BotValue.objects.create(bot=b, variable=_s, value="--not-entered--")
            del samples["variables"]

        for s, outputs in samples.items():
            _s, _ = Sample.objects.get_or_create(content=s, intention=_i)
            for o in outputs:
                _o, _ = OutputFormat.objects.get_or_create(output_format=o, sample=_s)

b.modules.set(modules)
b.save()