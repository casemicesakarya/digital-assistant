# Digital Assistant


### Install Tools

To start developing backend-end project make sure you have these tools on your machine:

- Python3.6
- [virtualenv](https://virtualenv.pypa.io/en/latest/)

### Install packages
If you run first time the project: create a virtual environment, install requirements and create a development database with the following script

```
sh utils/dev/setup.sh
```

### Runserver
Then activate the virtual environment:

```
source ../env/bin/activate
./manage.py runserver
```

### Then go to the Admin Panel:

[https://localhost:8000/admin](http://localhost:8000/admin)

```
email: superuser@casemice.com
password: 1
```
