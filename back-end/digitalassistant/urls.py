from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, re_path, include
from django.conf import settings

from .apis import GetToken
from .views import ApplicationView
from engine.urls import engine_router, engine_urlpatterns
from bots.urls import bots_router, bots_urlpatterns
from platforms.urls import platforms_urlpatterns

admin.site.site_url = None

def trigger_error(request):
    division_by_zero = 1 / 0

urlpatterns = [
	path('sentry-debug/', trigger_error),
	re_path(r"^$", ApplicationView.as_view(), name="Application"),
	re_path(r'^jet/', include('jet.urls', 'jet')),
	path('admin/', admin.site.urls),
	path('get-token/', GetToken.as_view(),name="get-token"),
]


urlpatterns += engine_router.urls
urlpatterns += engine_urlpatterns
urlpatterns += bots_router.urls
urlpatterns += bots_urlpatterns
urlpatterns += platforms_urlpatterns

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_title = "Digital Assistant"
