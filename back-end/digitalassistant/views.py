from django.views.generic import TemplateView


class ApplicationView(TemplateView):
	template_name = "application.html"
