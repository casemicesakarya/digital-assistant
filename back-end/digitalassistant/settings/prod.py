from .base import *


# SECURITY WARNING: don"t run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
	"default": {
		"ENGINE": "django.db.backends.postgresql_psycopg2",
		"NAME": "digitalassistant",
		"USER": "digitalassistant",
		"PASSWORD": "ag1SrzM8G22P",
		"HOST": "localhost",
		"PORT": "",
	}
}

INTERNAL_IPS = ["localhost", "127.0.0.1"]

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
	{ "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator", },
	{ "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator", },
	{ "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator", },
	{ "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator", },
]

ALLOWED_HOSTS = ["*"]

STATIC_ROOT = BASE_DIR + "/static/"

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="https://d72536f2aa2545e1be95b5c4f7537c81@o1009353.ingest.sentry.io/5973573",
    integrations=[DjangoIntegration()],

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0,

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True
)