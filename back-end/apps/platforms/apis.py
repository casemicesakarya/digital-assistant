import requests
from rest_framework.views import APIView
from django.http.response import JsonResponse
from bots.models import Bot, BotTriggerMessage, BotTriggerScenario
from engine.models import Trigger, Intention, Sample
from .models import Platform, PlatformUser

CORE_API_URL = "http://127.0.0.1:5000"


class ReceiveMessage(APIView):
    def post(self, request, format=None):
        # print(request.data.get('platfrom_token'))
        platform = Platform.objects.get(token=request.data.get('platform_token'))
        bot = Bot.objects.get(uuid=request.data.get('bot'))
        platformUser, created = PlatformUser.objects.get_or_create(platform=platform,bot=bot,user_id=request.data.get('user_id'),solution_id=request.data.get('solution_id'))
        # if api.save_history:
            # msg = Message(user=apiUser,text=request.data.get('text'),is_from_bot=False)
            # msg.save()
        r = requests.post(CORE_API_URL+ "/core/message/%s/"%bot.uuid, json=request.data)

        return JsonResponse({"status": "Message Received"},safe=False)


class SendMessage(APIView):

    def post(self, request, format=None):
        platform = Platform.objects.get(token=request.data.get('platform_token'))
        bot = Bot.objects.get(uuid=request.data.get('bot'))
        platformUser = PlatformUser.objects.get(platform=platform,bot=bot,user_id=request.data.get('user_id'),solution_id=request.data.get('solution_id'))
        # if platform.save_history:
        #     msg = Message(user=apiUser,text=request.data.get('text'),is_from_bot=False)
        #     msg.save()
        r = requests.post(platform.url,json={"user_id": platformUser.user_id, "solution_id":platformUser.solution_id  ,"message": request.data.get('text')})
        return JsonResponse({"status": "Message Sent"},safe=False)


class SendAndReceive(APIView):

    def post(self, request, format=None):
        platform = Platform.objects.get(token=request.data.get('platform_token'))
        bot = Bot.objects.get(uuid=request.data.get('bot'))
        platformUser, created = PlatformUser.objects.get_or_create(platform=platform,bot=bot,user_id=request.data.get('user_id'),solution_id=request.data.get('solution_id'))
        r = requests.post(CORE_API_URL+ "/core/message-request/%s/"%bot.uuid, json=request.data)
        json_response = r.json()
        if json_response['intent'] != -1:
            intent = Intention.objects.get(id=json_response['intent'])
            json_response["intent_title"] = intent.title
        else:
            json_response["intent_title"] = "UNK"

        if json_response["suggestions"]:
            json_response["suggestions"] = list(Sample.objects.filter(id__in=json_response["suggestions"]).values_list('content', flat=True))

        return JsonResponse(json_response, safe=False)


class SendTrigger(APIView):

    def post(self, request, format=None):
        platform = Platform.objects.get(token=request.data.get('platform_token'))
        bot = Bot.objects.get(uuid=request.data.get('bot'))
        platformUser = PlatformUser.objects.get(platform=platform,bot=bot,user_id=request.data.get('user_id'),solution_id=request.data.get('solution_id'))
        # if api.save_history:
        #     msg = Message(user=apiUser,text=request.data.get('text'),is_from_bot=False)
        #     msg.save()
        trigger = Trigger.objects.get(pk=request.data.get('trigger'))

        if BotTriggerMessage.objects.filter(trigger=trigger,bot=bot).exists():
            r = requests.post(platform.url,json={"user_id": platformUser.user_id, "solution_id":platformUser.solution_id, "message": request.data.get('text')})
            return JsonResponse({"status": "Message Sent"},safe=False)

        elif BotTriggerScenario.objects.filter(trigger=trigger,bot=bot).exists():
            r = requests.post(platform.scenario_url,json={"user_id": platformUser.user_id, "solution_id":platformUser.solution_id, "scenario": request.data.get('text')})
            return JsonResponse({"status": "Scenario Sent"},safe=False)
        else:
            HttpResponse(status=400)

class SendTimeTrigger(APIView):

    def post(self, request, format=None):
        bot = Bot.objects.get(uuid=request.data.get('uuid'))
        platformUsers = PlatformUser.objects.filter(bot=bot)
        trigger = Trigger.objects.get(pk=request.data.get('trigger'))
        for platformUser in platformUsers:
            # if apiUser.api.save_history:
            #     msg = Message(user=apiUser,text=request.data.get('text'),is_from_bot=True)
            #     msg.save()
            if BotTriggerMessage.objects.filter(trigger=trigger,bot=bot).exists():
                r = requests.post(platformUser.platform.url,json={"user_id": platformUser.user_id, "solution_id":platformUser.solution_id  ,"message": request.data.get('text')})
   
            elif BotTriggerScenario.objects.filter(trigger=trigger,bot=bot).exists():
                r = requests.post(platformUser.platform.scenario_url,json={"user_id": platformUser.user_id, "solution_id":platformUser.solution_id  ,"scenario": request.data.get('text')})

        return JsonResponse({"status": "Triggers Sent"},safe=False)
           