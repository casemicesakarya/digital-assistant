import uuid
from django.db import models
from digitalassistant.models import BaseModel
from django.utils.translation import ugettext_lazy as _
from bots.models import Bot


class Platform(BaseModel):
    name = models.CharField(max_length=100,verbose_name=_("Platform Name"))
    url = models.CharField(max_length=100,verbose_name=_("Platform url"))
    scenario_url = models.CharField(max_length=100,verbose_name=_("Platfrom Scenario url"),blank=True)
    token = models.UUIDField(default=uuid.uuid4, editable=False)
    
    def __str__(self):
        return (str(self.name))


class PlatformUser(BaseModel):
    bot = models.ForeignKey(Bot,related_name='platforms',on_delete=models.CASCADE)
    platform = models.ForeignKey(Platform,related_name='platform_users',on_delete=models.CASCADE,blank=True)
    user_id = models.CharField(max_length=20,verbose_name=_("Platfrom User ID"),blank=True)
    solution_id = models.CharField(max_length=20,verbose_name=_("Platform Solution ID"),blank=True,null=True)
