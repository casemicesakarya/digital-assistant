from django.conf.urls import url,include
from django.urls import path, re_path
from rest_framework import routers
from .apis import *

platforms_urlpatterns = [
    re_path(r"^receive-message",ReceiveMessage.as_view(), name="receive"),
    re_path(r"^send-message",SendMessage.as_view(), name="send"),
    re_path(r"^send-receive",SendAndReceive.as_view(), name="send_receive"),
    re_path(r"^send-trigger",SendTrigger.as_view(), name="trigger"),
    re_path(r"^send-time-trigger",SendTimeTrigger.as_view(), name="time_trigger"),
]