from django.contrib import admin
from .models import Platform, PlatformUser

# Register your models here.
class PlatformAdmin(admin.ModelAdmin):
    fields = ('name', 'url', 'scenario_url', 'token')
    readonly_fields = ('token',)

    def get_fields(self, request, obj=None):
        fields = list(super().get_fields(request, obj))
        if not obj:
            fields.remove('token')
        return fields

admin.site.register(Platform, PlatformAdmin)
admin.site.register(PlatformUser)