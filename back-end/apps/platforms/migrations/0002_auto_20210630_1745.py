# Generated by Django 2.2.7 on 2021-06-30 14:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('platforms', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='platformuser',
            old_name='platfrom',
            new_name='platform',
        ),
    ]
