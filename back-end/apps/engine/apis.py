from rest_framework import serializers, viewsets, permissions, authentication
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken, APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, action
from rest_framework.parsers import JSONParser, MultiPartParser
from .models import *
from .serializers import *
import json
import requests


class VariableViewSet(viewsets.ModelViewSet):
	queryset = Variable.objects.all()
	serializer_class = VariableSerializer

class OutputFormatViewSet(viewsets.ModelViewSet):
	queryset = OutputFormat.objects.all()
	serializer_class = OutputFormatSerializer

class SampleViewSet(viewsets.ModelViewSet):
	queryset = Sample.objects.all()
	serializer_class = SampleSerializer

	def perform_create(self, serializer):

		obj=serializer.save(
			content=self.request.data.get("content"),
			intention_id=self.request.data.get("intention")
		)
		output_formats = self.request.data.get("output_formats", None)
		if output_formats:
			for output_format in output_formats:
				of = OutputFormat.objects.create(output_format=output_format["output_format"], sample=obj)
				of.save()

	def perform_update(self, serializer):

		obj=serializer.save(
			content=self.request.data.get("content"),
			intention_id=self.request.data.get("intention")
		)
		output_formats = self.request.data.get("output_formats", None)
		if output_formats:
			OutputFormat.objects.filter(sample=obj).delete()
			for output_format in output_formats:
				of = OutputFormat.objects.create(output_format=output_format["output_format"], sample=obj)
				of.save()


class IntentionViewSet(viewsets.ModelViewSet):
	queryset = Intention.objects.all()
	serializer_class = IntentionSerializer

class ModuleViewSet(viewsets.ModelViewSet):
	queryset = Module.objects.all()
	serializer_class = ModuleSerializer

	def list(self, request):
		serializer = ModuleListSerializer(self.queryset, many=True)
		return Response(serializer.data)
	
	@action(detail=True,methods=['post'],url_path='add-trigger')
	def add_trigger(self,request,pk=None):
		module = self.get_object()
		for trigger in request.data.get('triggers'):
			trigger = Trigger.objects.get(pk=trigger)
			module.triggers.add(trigger)
		data = ModuleSerializer(self.get_object()).data
		return Response(data)
	
	@action(detail=True,methods=['post'],url_path='remove-trigger')
	def remove_trigger(self,requset,pk=None):
		module = self.get_object()
		for trigger in requset.data.get('triggers'):
			trigger = Trigger.objects.get(pk=trigger)
			module.triggers.remove(trigger)
		data = ModuleSerializer(self.get_object()).data
		return Response(data)


class TriggerViewSet(viewsets.ModelViewSet):
	queryset = Trigger.objects.all()
	serializer_class = TriggerSerializer

class ScenarioViewSet(viewsets.ModelViewSet):
	queryset = Scenario.objects.all()
	serializer_class = ScenarioSerializer