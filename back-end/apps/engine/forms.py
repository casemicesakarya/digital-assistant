from django import forms


class UploadModulesForm(forms.Form):

    ENGLISH = 'en'
    TURKISH = 'tr'

    LANGUAGES = (
        ('tr', 'Turkish'),
        ('en', 'English'),
    )

    file = forms.FileField(label='Dosya',required=True)
    language = forms.ChoiceField(label='Language', widget=forms.Select, choices=LANGUAGES)
