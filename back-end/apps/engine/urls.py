from django.conf.urls import url,include
from django.urls import path, re_path
from rest_framework import routers
from .apis import *
from .views import *


engine_router = routers.DefaultRouter()


engine_router.register('engine-api/variables', VariableViewSet)
engine_router.register('engine-api/outputformats',OutputFormatViewSet)
engine_router.register('engine-api/samples', SampleViewSet)
engine_router.register('engine-api/intentions', IntentionViewSet)
engine_router.register('engine-api/modules', ModuleViewSet)
engine_router.register('engine-api/triggers', TriggerViewSet)
engine_router.register('engine-api/scenarios', ScenarioViewSet)

engine_urlpatterns = [
    re_path(r"^core/", engine_view, name="core"),
    re_path(r"^similarity-check",SimilarityCheck.as_view(), name="similarity"),
    url(r'^create-scenario/(?P<id>\d+)/$', create_scenario, name='create-scenario'),
    url(r'data-editor/(?P<id>[0-9]+)/$', DataEditorView.as_view(), name='data-editors')
]
