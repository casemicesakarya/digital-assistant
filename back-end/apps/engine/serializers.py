from rest_framework import serializers
from .models import *

class VariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variable
        fields = ['id', 'name', 'intention']

class OutputFormatSerializer(serializers.ModelSerializer):
    class Meta:
        model = OutputFormat
        fields = ['id','output_format']

class SampleSerializer(serializers.ModelSerializer):
    sample_output_formats = OutputFormatSerializer(source='output_formats', many=True, required=False)
    intention_title = serializers.CharField(source='intention.title', read_only=True)

    class Meta:
        model = Sample
        fields = ['id','content', 'intention', 'intention_title', 'sample_output_formats']

class IntentionSerializer(serializers.ModelSerializer):
    intention_samples = SampleSerializer(source='samples', many=True,required=False)
    intention_variables = VariableSerializer(source='variables', many=True,required=False)

    class Meta:
        model = Intention
        fields = ['id','title','intention_samples','intention_variables']

class ModuleSerializer(serializers.ModelSerializer):
    module_intentions = IntentionSerializer(source='intentions', many=True,required=False)

    class Meta:
        model = Module
        fields = ['id','title','description','parent_module','module_intentions']

class ModuleListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Module
        fields = ['id','title','description','parent_module']

class TriggerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Trigger
        fields = ['id','title','trigger_type','interval','interval_variance','keywords','intention','event_date']

class ScenarioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Scenario
        fields = ['id','title','description','json']