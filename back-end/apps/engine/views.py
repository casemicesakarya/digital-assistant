import random
from django import forms
from django.shortcuts import render
from django.http.response import JsonResponse
from rest_framework.views import APIView
from django.shortcuts import redirect
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from openpyxl import load_workbook, Workbook
from django.contrib.auth import get_user_model
from datetime import datetime
from bots.models import *
from .models import *
from .forms import UploadModulesForm
import requests
import json


CORE_API_URL = "http://127.0.0.1:5000"
# Create your views here.
# da.com/core/build-bot/123il234h234d23 -> localhost:4000/core/build-bot/123il234h234d23
@csrf_exempt
def engine_view(request):
    # build-bot/bot_id
    # start-bot/bot_id
    # message/ post {"message": "some-text", "bot":"bot_id", "timestamp": "somedatetime". ..etc}
    # stop-bot/bot_id
    # check-similarity/bot_id
    requested_api = request.path

    if request.method == "GET":
        r = requests.get(CORE_API_URL + requested_api)
        return JsonResponse(json.loads(r.text), safe=False)
    elif request.method == "POST":
        r = requests.post(CORE_API_URL + requested_api, json=json.loads(request.body))
        return JsonResponse(r.json(), safe=False)


class DataEditorView(TemplateView):
	template_name = "data-editor.html"

	def get_context_data(self, **kwargs):
		context = super(DataEditorView, self).get_context_data(**kwargs)
		module_id = kwargs.get("id", None)
		context['current_module'] = int(module_id) if module_id else None
		return context


def create_scenario(request, id):
    start_node = ScenarioMptt.objects.get(pk=id)
    nodes = start_node.get_descendants(include_self=True)
    scenario_json = []
    for node in nodes:
        to_ids = []
        for child in node.children.all():
            to_ids.append(child.id)
        if len(to_ids) > 1:
            js = {
                "id": node.id,
                "to": to_ids,
                "type": node.node_type,
                "message": node.message
            }
        else:
            js = {
                "id": node.id,
                "to": child.id,
                "type": node.node_type,
                "message": node.message
            }
        scenario_json.append(js)
    scenario_json = json.dumps(scenario_json, indent=None, ensure_ascii=False, separators=(',', ':'))
    scenario = Scenario(title="new scenario", json=scenario_json)
    scenario.save()
    response = "/admin/engine/scenario/%s/change/"%scenario.id
    return HttpResponseRedirect(response)


class SimilarityCheck(APIView):
    def post(self, request, format=None):
        Bot.objects.filter(uuid=request.data.get('uuid')).update(similarity=request.data.get('similarity'),similarity_date = datetime.now())
        return HttpResponse(status=200)
