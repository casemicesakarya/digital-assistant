from django.db import models
from digitalassistant.models import BaseModel
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey
from bots.models import Bot, BotValue


class Variable(BaseModel):
	"""Variables are interpreted as answers or values to be placed in answers"""

	intention = models.ForeignKey('Intention',related_name='variables',on_delete=models.CASCADE,verbose_name=_("associated intention"))
	name = models.CharField(max_length=512,verbose_name=_("variable name"), help_text=_("Dinamik değişkenin adı , özel karakter, boşluk içermemelidir"))
	
	def __str__(self):
		return str(self.intention) + " | " + self.name

	def save(self, *args, **kwargs):
		created  = self._state.adding
		super(Variable, self).save(*args, **kwargs)
		if created:
			bots = Bot.objects.filter(modules=self.intention.module)
			for bot in bots:
				botValue = BotValue(bot=bot, variable=self)
				botValue.save()

	def delete(self, *args, **kwargs):
		bots = Bot.objects.filter(modules=self.intention.module)
		BotValue.objects.filter(bot__in=bots, variable=self).delete()
		return super(Variable, self).delete(*args, **kwargs)

	def get_output_formats(self):
		samples = Sample.objects.filter(intention=self.intention)
		output = OutputFormat.objects.filter(sample__in=samples, output_format__icontains=self.name).first()
		return output


class OutputFormat(BaseModel):
	"""
	An output is one of many outputs to a sample
	For languages like turkish language, some charachters needs to be added as a morphological format and this format depends on the value of the variable.
	to do this we use $ before the string that needs to be analyzed and formatted morphologically.
	
	Ex: "ben {sehir}l$Iy$Im." -> if sehir == "istanbul": returned value will be: "ben istanbulluyum."
	"""

	output_format = models.CharField(max_length=512,verbose_name=_("output format"), help_text=_("output format formats given variables into text: \"my name is {name}, I am {age} years old.\""))
	sample = models.ForeignKey('Sample',on_delete=models.CASCADE,related_name="output_formats",null=True,verbose_name=_("associated sample"))

	def __str__(self):
		return self.output_format


class Sample(BaseModel):
	"""A sample is an example of some statement or a question"""
	TURKISH = 'tr'
	ENGLISH = 'en'

	SAMPLE_LANGUAGE = (
		(TURKISH, 'Turkish'),
		(ENGLISH, 'English'),
	)

	content = models.CharField(max_length=512,verbose_name=_("content"))
	language = models.CharField(max_length=2,choices=SAMPLE_LANGUAGE,default=TURKISH,verbose_name=_("sample language"))
	intention = models.ForeignKey('Intention',related_name='samples',on_delete=models.SET_NULL,null=True,verbose_name=_("associated intention"))

	def __str__(self):
		return self.content


class Intention(BaseModel):
	"""Intention model, has 3 different types, title and samples associated with it"""
	QUESTION = 'Q'
	CONVENTIONAL_OPENING = 'C'
	STATEMENT = 'S'
	
	INTENTION_TYPE_CHOICES = (
		(QUESTION, 'Question'),
		(CONVENTIONAL_OPENING, 'Conventional opening'),
		(STATEMENT, 'Statement'),
	)

	title = models.CharField(max_length=200,verbose_name=_("title"))
	intention_type = models.CharField(max_length=1,choices=INTENTION_TYPE_CHOICES,default=QUESTION,verbose_name=_("intention type"))
	module = models.ForeignKey('Module',related_name='intentions',on_delete=models.CASCADE,blank=True,null=True,verbose_name=_("parent module"))

	def __str__(self):
		return self.title

	def get_variables(self):
		variables = Variable.objects.filter(intention=self)
		_vars = ""
		for variable in variables:
			_vars += variable.name + ','
		return _vars

class Module(BaseModel):
	"""Module model, a module is a group of intentions"""
	HIGH = 'H'
	NORMAL = 'N'
	LOW = 'L'
	
	TRIGGERING_PRIORITY = (
		(HIGH, 'High'),
		(NORMAL, 'Normal'),
		(LOW, 'Low'),
	)

	ENGLISH = 'en'
	TURKISH = 'tr'

	LANGUAGES = (
		(ENGLISH, 'English'),
		(TURKISH, 'Turkish'),
	)

	title = models.CharField(max_length=100,verbose_name=_("title"))
	language = models.CharField(max_length=30,choices=LANGUAGES,default=TURKISH,null=False,verbose_name=_("Language"))
	description = models.TextField(max_length=1000,blank=True,null=True,verbose_name=_("description"))
	parent_module = models.ForeignKey('Module',related_name='children_modules',on_delete=models.SET_NULL,blank=True,null=True,verbose_name=_("parent module"))


	def __str__(self):
		return self.title


# working types for the meanwhile are [chronic, keywords, inactive, manual, event] 
class Trigger(BaseModel):
	"""
	Trigger model, defines a trigger which may have various types
	set trigger type to inactive trigger.
		- Manual trigger activates manually from management page
		- Idle trigger activates when the user is Idle for some interval
		- Keywords trigger activates when the user mentions some keywords
		- Event trigger activates on particular date and time like some annual occasion
		- Inactive trigger does not activate
		- Data trigger activates when some variables matches some values
		- Chronic trigger activates periodically some time interval and some variance
	"""
	CHRONIC = 'C'
	EVENT = 'E'
	MANUAL = 'M'
	INTENTION = 'N'
	KEYWORDS = 'W'
	INACTIVE = 'A'
	
	TRIGGERING_TYPE = (
		(CHRONIC, 'Chronic'),
		(EVENT, 'Event'),
		(MANUAL, 'Manual'),
		(INTENTION, 'Intention'),
		(KEYWORDS, 'Keywords'),
		(INACTIVE, 'Inactive'),
	)

	title = models.CharField(max_length=100,verbose_name=_("title"))
	description = models.TextField(max_length=1000,blank=True,verbose_name=_("description"))
	trigger_type = models.CharField(max_length=1,choices=TRIGGERING_TYPE,default=INACTIVE,null=False,verbose_name=_("Trigger type"))

	# chronic, Idle uses interval too
	# interval for chronic triggers, variance is randomize activation time
	interval = models.PositiveIntegerField(default=0,blank=True,null=True,verbose_name=_("time interval in minutes"))
	interval_variance = models.PositiveIntegerField(default=0,blank=True,null=True,verbose_name=_("time interval variance in minutes"))

	# intention for intention triggers
	intention = models.ForeignKey(Intention,on_delete=models.CASCADE,blank=True,null=True,related_name="related_intention")
	# keywords for keywords type triggers
	keywords = models.CharField(max_length=1000,blank=True,null=True,verbose_name=_("keywords"),help_text=_("keywords separated by a comma \"sport,football\" ."))

	# event date for event type triggers
	event_date = models.DateTimeField(verbose_name=_("event date"),blank=True,null=True,help_text=_("the trigger will be activated on this date and time. "))

	# data_dict = data dictionary of variables from the profile of the user and their corresponding values 

	def __str__(self):
		return self.trigger_type + " | "  + self.title


class Mechanism(BaseModel):
	"""
	A mechanism is some action that can be call or executed like triggering some module, or starting some conversation scenario, or maybe sending emails..
	"""
	title = models.CharField(max_length=100,verbose_name=_("title"))
	description = models.TextField(max_length=1000,blank=False,verbose_name=_("description"))

	# TODO:
	# - add the functionality to be executed using this mechanism

	def __str__(self):
		return self.trigger_type + " | "  + self.title

class Scenario(BaseModel):

	title = models.CharField(max_length=100,verbose_name=_("title"))
	description = models.TextField(max_length=1000,blank=True,verbose_name=_("description"))
	json = models.TextField(blank=True,null=True,default='[]')

	def __str__(self):
		return self.title

class ScenarioMptt(MPTTModel):

	START = 'start'
	END = 'end'
	MESSAGE = 'message'
	INPUT = 'input'
	VARIABLE = 'variable'
	SELECTION = 'selection'
	OPTION = 'option'

	NODE_TYPE = (
		(START, 'start'),
		(END, 'end'),
		(MESSAGE, 'message'),
		(INPUT, 'input'),
		(VARIABLE, 'variable'),
		(SELECTION, 'selection'),
		(OPTION, 'option')
	)

	message = models.CharField(max_length=100, verbose_name=_("message"), null=True, blank=True)
	parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
	node_type = models.CharField(max_length=10,choices=NODE_TYPE,null=False,verbose_name=_("Node type"))

	def __str__(self):
		if self.message:
			return self.node_type + " | " + self.message
		else:
			return self.node_type
