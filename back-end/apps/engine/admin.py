import requests
from django.contrib import admin
from django.conf.urls import url
from django_mptt_admin.admin import DjangoMpttAdmin
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from openpyxl import load_workbook
from .models import Variable, Intention, Module, Trigger, Scenario, ScenarioMptt, Sample, OutputFormat
from .forms import UploadModulesForm


class VariableInline(admin.TabularInline):
	model = Variable
	fields = ('name',)
	show_change_link = True
	can_delete = True
	extra = 0


class IntentionInline(admin.TabularInline):
	model = Intention
	fields =  ('title','variables')
	readonly_fields = ('variables',)
	show_change_link = True
	extra = 0

	def variables(self, obj):
		return obj.get_variables()

	variables.empty_value_display = '???'


class ModuleAdmin(admin.ModelAdmin):
	list_display = ('title','description')
	fields = ('title', 'description', 'language')
	search_fields = ('title', )
	inlines = (IntentionInline, )
	change_form_template = 'admin/module/module_change_form.html'
	change_list_template = 'admin/module/module_change_list.html'

	class Media:
		js = (
			'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', # jquery 'js/myscript.js', # project static folder 'app/js/myscript.js', # app static folder
		)

	# def get_readonly_fields(self, request, obj=None):
	#     readonly_fields = list(super().get_readonly_fields(request, obj))
	#     if obj:
	#         readonly_fields.append('language')
	#     return readonly_fields

	def get_urls(self):
		urls = super(ModuleAdmin, self).get_urls()
		security_urls = [
			url(r'^upload-modules/$', self.admin_site.admin_view(self.upload_modules))
		]
		return security_urls + urls

	def upload_modules(self, request):
		if request.method == "POST":
			form = UploadModulesForm(request.POST, request.FILES)
			if form.is_valid():
				filehandle = request.FILES['file']
				try:
					wb = load_workbook(filehandle)
					ws = wb.worksheets[0]
					counter = 0
					for row in ws.iter_rows(min_row=2):
						if row[0].value is None:
							break
						module, created = Module.objects.get_or_create(title=row[0].value, language=request.POST.get('language'))
						intention, created = Intention.objects.get_or_create(title=row[1].value, module=module)
						if intention:
							sample, created = Sample.objects.get_or_create(content=row[2].value, intention=intention)
							if row[5].value:
								variables = row[5].value.split(',')
								for var in variables:
									variable, created = Variable.objects.get_or_create(name=var, intention=intention)
							if row[3].value:
								outputs = row[3].value.split('|')
								for out in outputs:
									output_format, created = OutputFormat.objects.get_or_create(output_format=out, sample=sample)
							else:
								outputs = OutputFormat.objects.filter(sample__intention=intention)
								for out in outputs:
									output_format, created = OutputFormat.objects.get_or_create(output_format=out, sample=sample)

				except Exception as e:
					return render(
						request,
						'upload_modules.html',
						{
							'form': form,
							'message':'Bir hata oluştu tekrar deneyin',
							'detail': str(e)
						}
				)
			return redirect('/admin/engine/module')

		else:
			form = UploadModulesForm()
			return render(request, 'upload_modules.html', {'form': form})


class IntentionAdmin(admin.ModelAdmin):
	inlines = (VariableInline,)
	fields = ("title",)

	def response_change(self, request, obj):
		if request.POST.get("_save", None):
			response = "/admin/engine/module/%s/change/#/tab/inline_0/"%obj.module_id
			return HttpResponseRedirect(response)
		return super().response_change(request, obj)

	def has_module_permission(self, request):
		return False


class TriggerAdmin(admin.ModelAdmin):
	list_display = ('title', 'description')
	fields = ('title', 'description', 'trigger_type')

	def get_fields(self, request, obj=None):
		fields = list(super().get_fields(request, obj))
		if obj:
			if obj.trigger_type == 'C':
				fields.append('interval')
				fields.append('interval_variance')
			elif obj.trigger_type == 'E':
				fields.append('event_date')
				fields.append('interval_variance')
			elif obj.trigger_type == 'N':
				fields.append('intention')
			elif obj.trigger_type == 'W':
				fields.append('keywords')

		return fields

class ScenarioAdmin(DjangoMpttAdmin):
	change_form_template = 'admin/scenario/scenario_change_form.html'


class SampleAdmin(admin.ModelAdmin):
	search_fields = ("content",)

	def has_module_permission(self, request):
		return False


admin.site.register(Module, ModuleAdmin)
admin.site.register(Intention, IntentionAdmin)
admin.site.register(Trigger, TriggerAdmin)
admin.site.register(Scenario)
admin.site.register(ScenarioMptt, ScenarioAdmin)
admin.site.register(Sample,SampleAdmin)
