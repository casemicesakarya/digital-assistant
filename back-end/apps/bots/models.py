from django.db import models
from digitalassistant.models import BaseModel
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from engine.models import *
import uuid


class Bot(BaseModel):

    ENGLISH = 'en'
    TURKISH = 'tr'

    LANGUAGES = (
        (ENGLISH, 'English'),
        (TURKISH, 'Turkish'),
    )
    """Bot model represent a bot"""
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=100,verbose_name=_("title"))
    language = models.CharField(max_length=30,choices=LANGUAGES,default=TURKISH,null=False,verbose_name=_("Language"))
    description = models.TextField(max_length=1000,blank=True,verbose_name=_("description"))
    modules = models.ManyToManyField("engine.Module",blank=True,verbose_name=_("modules"))
    triggers = models.ManyToManyField("engine.Trigger",related_name='trigger_bots',blank=True,verbose_name=_("triggers"))
    is_active = models.BooleanField(default=False)
    similarity = models.TextField(blank=True,null=True,default='[]')
    similarity_date = models.DateTimeField(blank=True,null=True)

    def __str__(self):
        return (self.title if self.is_active else " (deactivated) " + self.title) + ("" if self.modules.count() != 0 else " (empty)")


class BotValue(BaseModel):
    """Bot specific value, this values would be inserted in the output format of intentions"""
    bot = models.ForeignKey(Bot,related_name='variables',on_delete=models.CASCADE,verbose_name=_("associated bot"))
    variable = models.ForeignKey("engine.Variable",related_name='values',on_delete=models.CASCADE,verbose_name=_("variable (Intention | Variable Name)"))
    value = models.CharField(max_length=1000,verbose_name=_("variable value"),blank=True)

    def __str__(self):
        return ( str(self.bot) + " | " + str(self.variable) + " | " + self.value )

    def get_output_formats(self):
        return self.variable.get_output_formats()

class BotTriggerMessage(BaseModel):

    bot = models.ForeignKey(Bot,on_delete=models.CASCADE,related_name="bot_trigger_messages",verbose_name=_("associated bot"))
    trigger = models.ForeignKey("engine.Trigger",on_delete=models.CASCADE,related_name=("trigger_message"))
    message = models.CharField(max_length=1000,verbose_name=_("trigger message"),blank=True)

    class Meta:
        unique_together = ('bot', 'trigger',)

    def clean(self):
        if BotTriggerScenario.objects.filter(bot=self.bot, trigger=self.trigger).count():
            raise ValidationError('This trigger is already associated with a message')

class BotTriggerScenario(BaseModel):

    bot = models.ForeignKey(Bot, on_delete=models.CASCADE,related_name="bot_trigger_scenarios")
    trigger = models.ForeignKey("engine.Trigger",on_delete=models.CASCADE,related_name="trigger_scenario")
    scenario = models.ForeignKey("engine.Scenario",on_delete=models.CASCADE,related_name="related_scenario")

    class Meta:
        unique_together = ('bot', 'trigger',)

    def clean(self):
        if BotTriggerMessage.objects.filter(bot=self.bot, trigger=self.trigger).count():
            raise ValidationError('This trigger is already associated with a scenario')


class Api(BaseModel):
    name = models.CharField(max_length=1000,verbose_name=_("Platform Name"))
    url = models.CharField(max_length=1000,verbose_name=_("Api url"))
    url_scenario = models.CharField(max_length=1000,verbose_name=_("Api Scenario url"),blank=True)
    save_history = models.BooleanField(default=False)
    def __str__(self):
        return (str(self.name))

class ApiUser(BaseModel):
    bot = models.ForeignKey(Bot,related_name='api_user_related_bot',on_delete=models.CASCADE)
    api = models.ForeignKey(Api,related_name='related_api',on_delete=models.CASCADE,blank=True,null=True)
    user_id = models.CharField(max_length=1000,verbose_name=_("Api User ID"),blank=True)
    platform_id = models.CharField(max_length=1000,verbose_name=_("Api Platform ID"),blank=True)

class Message(BaseModel):
    user = models.ForeignKey(ApiUser,related_name='related_user',on_delete=models.CASCADE)
    text = models.CharField(max_length=1000,verbose_name=_("Message text"))
    is_from_bot = models.BooleanField(verbose_name=_("sender"))
