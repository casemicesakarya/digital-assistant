from rest_framework import serializers, viewsets, permissions, authentication
from rest_framework.authtoken.models import Token
from django.http.response import JsonResponse
from rest_framework.authtoken.views import ObtainAuthToken, APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, action
from rest_framework.parsers import JSONParser,MultiPartParser
from .models import *
from engine.models import *
from .serializers import *
import json
import requests


class BotViewSet(viewsets.ModelViewSet):
	queryset = Bot.objects.all()
	serializer_class = BotSerializer

	@action(detail=True,methods=['post'],url_path='add-module')
	def add_module(self,request,pk=None):
		bot = self.get_object()
		for module in request.data.get('modules'):
			intentions = Intention.objects.filter(module=module)
			intention_ids = [intention.id for intention in intentions]
			variables = Variable.objects.filter(intention__in=intention_ids)
			for variable in variables:
				botValue = BotValue(bot=bot,variable=variable)
				botValue.save()
		data = BotSerializer(self.get_object()).data
		return Response(data)
	
	@action(detail=True,methods=['post'],url_path='remove-module')
	def remove_module(self,requset,pk=None):
		bot = self.get_object()
		for module in requset.data.get('modules'):
			intentions = Intention.objects.filter(module=module)
			intention_ids = [intention.id for intention in intentions]
			variables = Variable.objects.filter(intention__in=intention_ids)
			variable_ids = [variable.id for variable in variables]
			BotValue.objects.filter(variable__in=variable_ids,bot=bot).delete()
		data = BotSerializer(self.get_object()).data
		return Response(data)
	
	@action(detail=True,methods=['post'],url_path='add-trigger')
	def add_trigger(self,request,pk=None):
		bot = self.get_object()
		for trigger in request.data.get('triggers'):
			trigger = Trigger.objects.get(pk=trigger)
			botTriggerMessage = BotTriggerMessage(bot=bot,trigger=trigger)
			botTriggerMessage.save()
		data = BotSerializer(self.get_object()).data
		return Response(data)

	@action(detail=True,methods=['post'],url_path='remove-trigger')
	def remove_trigger(self,requset,pk=None):
		bot = self.get_object()
		for trigger in requset.data.get('triggers'):
			trigger = Trigger.objects.get(pk=trigger)
			BotTriggerMessage.objects.filter(trigger=trigger,bot=bot).delete()
		data = BotSerializer(self.get_object()).data
		return Response(data)
	
	@action(detail=False,methods=['post'],url_path='get-bot')
	def get_bot(self,request,pk=None):
		bot = Bot.objects.get(uuid=request.data.get('uuid'))
		data = BotDetailedSerializer(instance=bot).data
		return Response(data)
	
	@action(detail=False,methods=['post'],url_path='activate-bot')
	def activate_bot(self,request,pk=None):
		bot = Bot.objects.get(uuid=request.data.get('uuid'))
		bot.is_active = True
		bot.save()
		return JsonResponse({"status": "Successs"},safe=False)

	@action(detail=False,methods=['post'],url_path='deactivate-bot')
	def deactivate_bot(self,request,pk=None):
		bot = Bot.objects.get(uuid=request.data.get('uuid'))
		bot.is_active = False
		bot.save()
		return JsonResponse({"status": "Successs"},safe=False)

	@action(detail=True,methods=['get'],url_path='get-similarity-check')
	def get_similarity_check(self,request,pk=None):
		bot = self.get_object()
		sim = eval(bot.similarity)
		sim_table = []
		for item in sim:
			try:
				intent_1 = Intention.objects.get(pk=item.get('Intent-1'))
				intent_2 = Intention.objects.get(pk=item.get('Intent-2'))
			except Intention.DoesNotExist:
				print("Intention does not exsist")
				continue
			try:
				sample_1 = Sample.objects.get(pk=item.get('Sample-1-id'))
				sample_2 = Sample.objects.get(pk=item.get('Sample-2-id'))
			except Sample.DoesNotExist:
				print("Sample does not exsist")
				continue

			sim_table.append({
				"ratio": item.get('ratio'),
			 	"intent1": intent_1.title,
				"intent1_id": intent_1.id,
				"intent1_module": intent_1.module.id,
			 	"sample1": sample_1.content,
			 	"intent2": intent_2.title,
				"intent2_id": intent_2.id,
				"intent2_module": intent_2.module.id,
			 	"sample2": sample_2.content,
			})			
		obj = {"similarity": sim_table, "similarity_date": bot.similarity_date}
		return Response(obj)


class BotValueViewSet(viewsets.ModelViewSet):
	queryset = BotValue.objects.all()
	serializer_class = BotValueSerializer

	@action(detail=False, methods=['post'],url_path='get-variables-by-module')
	def get_variables_by_module(self, request):
		intentions = Intention.objects.filter(module=request.data.get('module'))
		intention_ids = [intention.id for intention in intentions]
		variables = Variable.objects.filter(intention__in=intention_ids)
		botValues = BotValue.objects.filter(variable__in=variables,bot=request.data.get('bot'))
		data = BotValueSerializer(botValues,many=True).data
		return Response(data)


class BotTriggerMessageViewSet(viewsets.ModelViewSet):
	
	queryset = BotTriggerMessage.objects.all()
	serializer_class = BotTriggerMessageSerializer

	@action(detail=False, methods=['post'],url_path='get-message')
	def get_trigger_message_by_trigger(self, request):
		trigger = Trigger.objects.get(pk=request.data.get('trigger'))
		bot = Bot.objects.get(pk=request.data.get('bot'))
		botTriggerMessages = BotTriggerMessage.objects.get(trigger=trigger,bot=bot)
		data = BotTriggerMessageSerializer(instance=botTriggerMessages).data
		return Response(data)


class MessageViewSet(viewsets.ModelViewSet):
	queryset = Message.objects.all()
	serializer_class = MessageSerializer
	authentication_classes = (authentication.TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)

	@action(detail=False, methods=['post'],url_path='get-messages')
	def get_messages(self, request):
		botUserMessages = Message.objects.filter(user=request.data.get('user')).order_by('-create_date')
		data = MessageSerializer(botUserMessages,many=True).data
		return Response(data)
