from rest_framework import serializers
from engine.serializers import *
from .models import *


class BotValueSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='variable.name',read_only=True)
    intention = serializers.CharField(source='variable.intention',read_only=True)
    class Meta:
        model = BotValue
        fields = ['id','value','bot','variable','name','intention']

class BotTriggerMessageSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='trigger.title',read_only=True)
    related_trigger = TriggerSerializer(source='trigger',read_only=True)
    class Meta:
        model = BotTriggerMessage
        fields = ['id','message','bot','related_trigger','title']

class BotTriggerScenarioSerializer(serializers.ModelSerializer):
    related_trigger = TriggerSerializer(source='trigger',read_only=True)
    bot_scenario = ScenarioSerializer(source='scenario',read_only=True)
    class Meta:
        model = BotTriggerScenario
        fields = ['id', 'related_trigger','bot_scenario']

class BotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bot
        fields = ['id','uuid','title','description','modules','triggers','is_active']

class BotDetailedSerializer(serializers.ModelSerializer):
    bot_modules = ModuleSerializer(source='modules',many=True)
    bot_triggers = BotTriggerMessageSerializer(source='bot_trigger_messages',many=True)
    bot_values = BotValueSerializer(source='variables', many=True)
    bot_scenarios = BotTriggerScenarioSerializer(source='bot_trigger_scenarios',many=True)
    class Meta:
        model = Bot
        fields = ['id','uuid','title','language','description','is_active','bot_modules','bot_triggers','bot_values','bot_scenarios']

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

class ApiUser(serializers.ModelSerializer):
    class Meta:
        model = ApiUser
        fields = ['id', 'url', 'platform_id']