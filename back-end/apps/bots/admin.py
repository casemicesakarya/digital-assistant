import json
import requests
import copy
from django.contrib import admin
from django.shortcuts import render, redirect
from django.conf.urls import url
from django.utils.timezone import now
from django.http import HttpResponse
from django.forms import TextInput, Textarea
from django.db import models
from openpyxl import load_workbook, Workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.styles import PatternFill, Font

from .forms import AnalyzeBotForm
from .models import Bot, BotValue, BotTriggerMessage, BotTriggerScenario, Api, ApiUser
from engine.models import Module, Intention, Variable


class BotVariableInline(admin.TabularInline):
	model = BotValue
	fields = ('variable', 'value', 'outputformats')
	ordering = ("variable__intention_id",)
	readonly_fields = ('variable', 'outputformats')
	formfield_overrides = {
        models.CharField: {'widget': Textarea(attrs={'rows':4, 'cols':40})},
        models.TextField: {'widget': Textarea(attrs={'rows':4, 'cols':40})},
    }

	def has_add_permission(self, request): 
		return False
	
	def has_delete_permission(self, request, obj): 
		return False

	def outputformats(self, obj):
		return obj.get_output_formats()

	outputformats.empty_value_display = '???'

class BotTriggerMessageInline(admin.TabularInline):
	model = BotTriggerMessage
	fields = ('trigger', 'message')
	extra = 1


class BotTriggerScenarioInline(admin.TabularInline):
	model = BotTriggerScenario
	fields = ('trigger', 'scenario')
	extra = 1


def copy_bot(modeladmin, request, queryset):
	for bot in queryset:
		bot_copy = copy.copy(bot)
		bot_copy.id = None
		bot_copy.title = "%s (copy)"%bot.title
		bot_copy.description = "%s (copy)"%bot.description
		bot_copy.save()

		for module in bot.modules.all():
			bot_copy.modules.add(module)

		for trigger in bot.triggers.all():
			bot_copy.triggers.add(trigger)

		botValues = BotValue.objects.filter(bot=bot)
		for botValue in botValues:
			botv = BotValue(bot=bot_copy, variable=botValue.variable, value=botValue.value)
			botv.save()

		botTriggerMessages = BotTriggerMessage.objects.filter(bot=bot)
		for botTriggerMessage in botTriggerMessages:
			botTM = BotTriggerMessage(bot=bot_copy, trigger=botTriggerMessage.trigger, message=botTriggerMessage.message)
			botTM.save()

		botTriggerScenarios = BotTriggerScenario.objects.filter(bot=bot)
		for botTriggerScenario in botTriggerScenarios:
			botTS = BotTriggerScenario(bot=bot_copy, trigger=botTriggerScenario.trigger, scenario=botTriggerScenario.scenario)
			botTS.save()

copy_bot.short_description = "copy bot"


class BotAdmin(admin.ModelAdmin):
	change_form_template = 'admin/bot/bot_change_form.html'
	inlines = (BotVariableInline, BotTriggerMessageInline, BotTriggerScenarioInline)
	fields = ('uuid', 'title', 'language', 'description', 'modules', 'is_active', 'similarity_date')
	filter_horizontal = ('modules', )
	readonly_fields = ('uuid', 'is_active', 'similarity_date')
	actions = (copy_bot, )
	bot_language = None

	def get_fields(self, request, obj=None):
		fields = list(super().get_fields(request, obj))
		if not obj:
			fields.remove('modules')
		return fields

	def get_form(self, request, obj=None, **kwargs):
		if obj:
			self.bot_language = obj.language
		return super(BotAdmin, self).get_form(request, obj, **kwargs)

	def get_readonly_fields(self, request, obj=None):
		readonly_fields = list(super().get_readonly_fields(request, obj))
		if obj:
			readonly_fields.append('language')
		return readonly_fields

	def formfield_for_manytomany(self, db_field, request, **kwargs):
		if db_field.name == "modules":
			kwargs["queryset"] = Module.objects.filter(language=self.bot_language)
		return super(BotAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

	def save_model(self, request, obj, form, change):
		if change:
			if 'modules' in form.changed_data:
				bot_variables = []
				for module in form.cleaned_data['modules']:
					intention_ids =  Intention.objects.filter(module=module).values_list("id", flat=True)
					variables = Variable.objects.filter(intention__in=intention_ids)
					for variable in variables:
						bot_value, created = BotValue.objects.get_or_create(bot=obj, variable=variable)
						bot_variables.append(bot_value)
				for bot_value in BotValue.objects.filter(bot=obj):
					if bot_value not in bot_variables:
						bot_value.delete()
			
			# if 'triggers' in form.changed_data:
			# 	bot_triggers = []
			# 	for trigger in form.cleaned_data['triggers']:
			# 		bot_trigger, created = BotTriggerMessage.objects.get_or_create(bot=obj, trigger=trigger)
			# 		bot_triggers.append(bot_trigger)
			# 	for bt in BotTriggerMessage.objects.filter(bot=obj):
			# 		if bt not in bot_triggers:
			# 			bt.delete()
		super().save_model(request, obj, form, change)

admin.site.register(Bot, BotAdmin)
# admin.site.register(Api)