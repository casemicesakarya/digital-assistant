import requests
import json
from datetime import datetime
from django import forms
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.utils.timezone import now
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import ListView
from openpyxl import load_workbook, Workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.styles import PatternFill, Font

from django.views import View
from .models import Bot
from engine.models import *

CORE_API_URL = "http://127.0.0.1:5000"

def build_bot(request, id):
	obj = Bot.objects.get(pk=id)
	obj.is_active = False
	obj.save()
	r = requests.get(CORE_API_URL + '/core/build-bot/' + str(obj.uuid))
	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def build_bot_triggers(request, id):
    obj = Bot.objects.get(pk=id)
    r = requests.get(CORE_API_URL + '/core/build-bot-triggers/' + str(obj.uuid))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def start_similarity_check(request, id):
    obj = Bot.objects.get(pk=id)
    obj.similarity_date = datetime.now()
    obj.save()
    r = requests.get(CORE_API_URL + '/core/check-similarity/' + str(obj.uuid))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def start_bot(request, id):
    obj = Bot.objects.get(pk=id)
    r = requests.get(CORE_API_URL + '/core/start-bot/' + str(obj.uuid))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def stop_bot(request, id):
    obj = Bot.objects.get(pk=id)
    r = requests.get(CORE_API_URL + '/core/stop-bot/' + str(obj.uuid))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def similarity_check_view(request, id):
    obj = Bot.objects.get(pk=id)
    similarity = json.loads(obj.similarity)
    data = {"data": similarity}
    return render(request, "similarity_check.html", data)


class AnalyzeBotForm(forms.Form):
	file = forms.FileField(label='Dosya', required=True)
	last_row = forms.IntegerField(label='Son satır no', required=False)



@login_required(login_url='/admin/login/')
def analyze_bot(request, bot_id):
	if not request.user.is_superuser:
		return redirect('/admin')
	if request.method == "POST":
		form = AnalyzeBotForm(request.POST, request.FILES)
		if form.is_valid():
			filehandle = request.FILES['file']
			# max_row = int(request.POST.get('last_row'))
			bot = Bot.objects.filter(id=bot_id).last()

			try:
				wb = load_workbook(filehandle)
				ws = wb.worksheets[0]

				# categories = {}
				# for intention in bot.intentions:
				# 	intentions[str(intention.intention_id)] = intention

				data = []
				for row in ws.iter_rows(min_row=2):
					if row[0].value is None:
						break
					message = row[0].value
					req_data = {
						"message": message
					}
					r = requests.post(CORE_API_URL+ "/core/analayze-message/%s/"%bot.uuid, json=req_data)
					output = r.json().get('output')
					sample_id = r.json().get('sample')
					module_id = r.json().get('module')
					intention_id = r.json().get('intent')
					confidence_level = r.json().get('confidence')

					if sample_id != -1:
						sample = Sample.objects.get(pk=sample_id)
						intention = Intention.objects.get(pk=intention_id)
						module = Module.objects.get(pk=module_id)
						data = data + [[
							message,
							sample.content,
							output,
							module.title,
							intention.title,
							confidence_level,
						]]

					elif sample_id == -1:
						data = data + [[message, "Bilinmeyen Categori", "Sizi anlayamadım", "UNK", "UNK", "None"]]

					else:
						data = data + [[message, "-", "-"]]

				wb_download = Workbook()
				ws = wb_download.active
				now_date = now().strftime("%d %m %Y")
				ws.title = now_date

				ws.append([
					"Gelen Soru", "Anlaşılan Soru", "Cevap", "Module", "Intention", "Confidence Level"
				])
				for d in data:
					ws.append(d)

				for cell in ["A1", "B1", "C1", "D1", "E1", "F1"]:
					ws[cell].font = Font(bold=True, size=10, color = "FFFFFF")
					ws[cell].fill = PatternFill(fill_type="solid", fgColor="4589ff")

				ws.column_dimensions["A"].width = 50
				ws.column_dimensions["B"].width = 50
				ws.column_dimensions["C"].width = 40

				r = HttpResponse(save_virtual_workbook(wb_download), content_type='application/vnd.ms-excel')
				r['Content-Disposition'] = "attachment; filename=\"%s_%s.xls\""%(bot.title, now_date)
				return r

			except Exception as e:
				return render(
					request,
					'analyze_bot.html',
					{
						'bot_id': bot.id,
						'form': form,
						'message':'Bir hata oluştu tekrar deneyin',
						'detail': str(e)
					}
				)

	else:
		form = AnalyzeBotForm()
		return render(request, 'analyze_bot.html', {'form': form})
