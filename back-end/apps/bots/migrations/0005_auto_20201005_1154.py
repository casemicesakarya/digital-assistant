# Generated by Django 2.2.7 on 2020-10-05 08:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bots', '0004_botscenario'),
    ]

    operations = [
        migrations.AlterField(
            model_name='botscenario',
            name='bot',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='scenarios', to='bots.Bot'),
        ),
        migrations.CreateModel(
            name='ApiUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateTimeField(auto_now_add=True, null=True, verbose_name='create date')),
                ('update_date', models.DateTimeField(auto_now=True, null=True, verbose_name='update date')),
                ('url', models.CharField(max_length=1000, verbose_name='Api url')),
                ('platform_id', models.CharField(max_length=1000, verbose_name='Api ID')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
