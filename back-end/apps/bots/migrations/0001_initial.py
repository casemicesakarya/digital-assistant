# Generated by Django 2.2.7 on 2020-09-11 11:06

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateTimeField(auto_now_add=True, null=True, verbose_name='create date')),
                ('update_date', models.DateTimeField(auto_now=True, null=True, verbose_name='update date')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('title', models.CharField(max_length=100, verbose_name='title')),
                ('description', models.TextField(blank=True, max_length=1000, verbose_name='description')),
                ('is_active', models.BooleanField(default=False)),
                ('similarity', models.TextField(blank=True, default='[]', null=True)),
                ('similarity_date', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BotTriggerMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateTimeField(auto_now_add=True, null=True, verbose_name='create date')),
                ('update_date', models.DateTimeField(auto_now=True, null=True, verbose_name='update date')),
                ('message', models.CharField(blank=True, max_length=1000, verbose_name='trigger message')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BotValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateTimeField(auto_now_add=True, null=True, verbose_name='create date')),
                ('update_date', models.DateTimeField(auto_now=True, null=True, verbose_name='update date')),
                ('value', models.CharField(blank=True, max_length=1000, verbose_name='variable value')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateTimeField(auto_now_add=True, null=True, verbose_name='create date')),
                ('update_date', models.DateTimeField(auto_now=True, null=True, verbose_name='update date')),
                ('text', models.CharField(max_length=1000, verbose_name='Message text')),
                ('is_from_bot', models.BooleanField(verbose_name='sender')),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bot', to='bots.Bot')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
