# Generated by Django 2.2.7 on 2020-10-21 11:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bots', '0006_apiuser_bot'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='apiuser',
            name='bot',
        ),
    ]
