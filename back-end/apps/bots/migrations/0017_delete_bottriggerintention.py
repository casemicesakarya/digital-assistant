# Generated by Django 2.2.7 on 2020-11-19 14:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bots', '0016_auto_20201119_1553'),
    ]

    operations = [
        migrations.DeleteModel(
            name='BotTriggerIntention',
        ),
    ]
