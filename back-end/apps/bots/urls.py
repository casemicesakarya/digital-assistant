from django.conf.urls import url,include
from rest_framework import routers
from .views import *
from .apis import *


bots_router = routers.DefaultRouter()

bots_router.register('bots-api/bots', BotViewSet)
bots_router.register('bots-api/bot-value',BotValueViewSet)
bots_router.register('bots-api/trigger-messages',BotTriggerMessageViewSet)
bots_router.register('bots-api/chat-histroy',MessageViewSet)

bots_urlpatterns = [
    url(r'^build-bot/(?P<id>\d+)/$', build_bot, name='build-bot'),
    url(r'^build-bot-triggers/(?P<id>\d+)/$', build_bot_triggers, name='build-bot-triggers'),
    url(r'^start-similarity-check/(?P<id>\d+)/$', start_similarity_check, name='start-similarity-check'),
    url(r'^show-similarity-check/(?P<id>\d+)/$', similarity_check_view, name='show-similarity-check'),
    url(r'^start-bot/(?P<id>\d+)/$', start_bot, name='start-bot'),
    url(r'^stop-bot/(?P<id>\d+)/$', stop_bot, name='stop-bot'),
    url(r'^admin/analyze-bot/(?P<bot_id>[0-9]+)$', analyze_bot , name='analyze_bot'),
]