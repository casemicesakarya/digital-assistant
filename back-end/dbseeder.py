# run using :
# $> python manage.py shell < dbseeder.py

import json
from engine.models import *
from bots.models import *

with open("utils/fixtures/msd_modules_seed.json", "r") as fi:
    seed_object = json.loads(fi.read())

with open("utils/fixtures/bot_as_seed.json") as fi:
    as_object = json.loads(fi.read())

with open("utils/fixtures/bot_ra_seed.json") as fi:
    ra_object = json.loads(fi.read())

bot_as, _ = Bot.objects.get_or_create(title="AS")
bot_ra, _ = Bot.objects.get_or_create(title="RA")
modules = []

for mod, ints in seed_object.items():
    _m = Module.objects.create(title=mod)
    modules.append(_m)
    for intention, samples in ints.items():
        _i = Intention.objects.create(title=intention, module=_m)
        if samples.get("variables", None):
            for v in samples["variables"]:
                _v = Variable.objects.create(name=v, intention=_i)
                _bv = BotValue.objects.create(bot=bot_as, variable=_v, value=as_object[intention])
                _bv = BotValue.objects.create(bot=bot_ra, variable=_v, value=ra_object[intention])
            del samples["variables"]

        for s, outputs in samples.items():
            _s = Sample.objects.create(content=s, intention=_i)
            for o in outputs:
                _o = OutputFormat.objects.create(output_format=o, sample=_s)

bot_as.modules.set(modules)
bot_ra.modules.set(modules)
bot_as.save()
bot_ra.save()