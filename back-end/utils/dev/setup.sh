rm -rf ../env &&
virtualenv -p python3.6 ../env &&
. ../env/bin/activate &&
pip install -r ./requirements/dev.txt &&
rm -rf ./db.sqlite3 &&
find . -name '*.pyc' -delete &&
./manage.py migrate &&
./manage.py loaddata ./utils/fixtures/core.json &&
./manage.py shell < smallseeder.py
