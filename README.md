# digital-assistant

## Install mongodb on mac osx

```shell
brew tap mongodb/brew
brew install mongodb-community@4.2
```

Start mongodb server as a service

```shell
brew services start mongodb-community@4.2
```

---------

## Django app installation and running

```shell
$ pip install requirements/dev.txt
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py shell < dbseeder.py
$ python manage.py runserver
```

---------

## Engine

### Models

- Using this format -> {some_variable} inside the output string, helps the engine to fetch this variable from the current chatbots data.

- For languages like turkish language, some charachters needs to be added as a morphological format and this format depends on the value of the variable. to do this we use $ before the string that needs to be analyzed and formatted morphologically.

- In case of a sample containing more than one output format the engine will choose one of them randomly. and if a sample did not contain any output format the engine will choose the first output format of the associated intention",
