import flask
from flask import request, jsonify
books = [
    {'id': 0,
     'title': 'A Fire Upon the Deep',
     'author': 'Vernor Vinge',
     'first_sentence': 'The coldsleep itself was dreamless.',
     'year_published': '1992'},
    {'id': 1,
     'title': 'The Ones Who Walk Away From Omelas',
     'author': 'Ursula K. Le Guin',
     'first_sentence': 'With a clamor of bells that set the swallows soaring, the Festival of Summer came to the city Omelas, bright-towered by the sea.',
     'published': '1973'},
    {'id': 2,
     'title': 'Dhalgren',
     'author': 'Samuel R. Delany',
     'first_sentence': 'to wound the autumnal city.',
     'published': '1975'}
]
message = {
        'message' : {
            'response': 'hello there'
        }
    }


start = [
    {
        'response': 'bot started'
    }
]

stop = [
    {
        'response': 'bot-stopped'
    }
]
build = [
    {
        'reponse': 'bot built successfully'
    }
]

status = {
    'response': 'running'
}


app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    return '''<h1>Distant Reading Archive</h1>
<p>A prototype API for distant reading of science fiction novels.</p>'''


# A route to return all of the available entries in our catalog.
@app.route('/core/books/all', methods=['GET'])
def api_all():
    return jsonify(books)

@app.route('/core/message', methods=['POST'])
def api_post():
    return message

@app.route('/core/start-bot/<bot_uuid>', methods=['GET'])
def api_start(bot_uuid):
    return jsonify(start)

@app.route('/core/stop-bot/<bot_uuid>', methods=['GET'])
def api_stop(bot_uuid):
    return jsonify(stop)

@app.route('/core/build-bot/<bot_uuid>', methods=['GET'])
def api_build(bot_uuid):
    return jsonify(build)

@app.route('/core/bot-status/<bot_uuid>', methods=['GET'])
def api_status(bot_uuid):
    return status

app.run()