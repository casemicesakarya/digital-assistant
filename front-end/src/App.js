import React, { Component } from 'react';
import {Route, Switch, Redirect, HashRouter} from 'react-router-dom'
import Modules from './components/Modules'
import Intentions from './components/Intentions'
import Triggers from './components/Triggers'
import Bots from './components/Bots'
import TestPlatform from './components/TestPlatform'
import Login from './components/Login'
import DefaultLayout from './layouts'
import Cookie from 'universal-cookie'
import './App.css';
const cookie = new Cookie()

class App extends Component {
  render() {
    return (
      <HashRouter>
        <Switch> 
          <Route path="/login" component={Login} /> 
          <DefaultLayout  >
            <Switch>
              <Route  exact path="/" render={()=><Redirect to='/modules'/>} />
              <Route path="/login" component={Login} />
              <Route path="/modules" component={Modules}/>
              <Route path="/module-intentions/:moduleId" component={Intentions}/>
              <Route path="/triggers" component={Triggers}/>
              <Route path="/bots" component={Bots}/>
              <Route path={"/test-platform"} component={TestPlatform}/> 
              <Route render={()=> <h3>Not Found</h3> }></Route>
            </Switch>
          </DefaultLayout>
        </Switch>
      </HashRouter>
    );
  }
}

export default App;
