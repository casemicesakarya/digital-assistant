import React  from 'react';
import PropTypes from 'prop-types';
import { Link} from 'react-router-dom'
import axios from 'axios'
import useStyles from './Styles.js'
import { Container, Typography, Button, Grid, List, ListItem,
         ListItemText, ListSubheader, InputLabel, Divider, TextField, 
         Card, CardContent, FormControl  } from '@material-ui/core';
import MaterialTable from 'material-table';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import EjectIcon from '@material-ui/icons/Eject';
const config = require('../config.js')


const WAIT_INTERVAL = 1000;

function Intention(props){
  
  if (props.intention.id === undefined){
    return null
  }

  const classes = useStyles();
  const [intentionName, setIntentionName] = React.useState('')
  const [checked, setChecked] = React.useState([])
  const [intentionVariables, setIntentionVariables] = React.useState([])
  const [samples, setSamples] = React.useState([])
  const [samplesData, setSamplesData] = React.useState([])
  const [outputFormats,setOutputFormats] = React.useState([])
  const [formatsData, setFormatsData] = React.useState([])
  const [selectedFormats, setSlectedFormats] = React.useState([])
  const [isPropsUpdate, setIsPropsUpdate] = React.useState(true)

  React.useEffect(() => {
    setIsPropsUpdate(true)
    axios.get(`${config.digitalAssistantApi}/engine-api/intentions/${props.intention.id}/`)
      .then((response) => {
        setSamples(response.data.intention_samples)
        setSamplesData(response.data.intention_samples.map((item) =>{
                    return({content: item.content, id: item.id})
                  }))
        setIntentionName(response.data.title)
        setIntentionVariables(response.data.intention_variables)
      })
    setChecked([])
    setOutputFormats([])
    setFormatsData([])
    setSlectedFormats([])
  },[props.intention])

  React.useEffect(() =>{
    var sample = checked[0] ? samples.find(x => x.id == checked[0].id) : null
    setSlectedFormats([])
    if (sample){
      setOutputFormats(sample.sample_output_formats)
      setFormatsData(sample.sample_output_formats.map((item) =>{
        return({output_format: item.output_format, id: item.id})
      }))
    }
    else {
      setOutputFormats([])
      setFormatsData([])
    }
  },[checked])

  React.useEffect(() =>{
    setChecked([])
    setSamplesData(samples.map((item) =>{
      return({content: item.content, id: item.id})
    }))
  },[samples])

  React.useEffect(() =>{
  //  if (checked[0]){
    setSlectedFormats([])
    setFormatsData(outputFormats.map((item) =>{
      return({output_format: item.output_format, id: item.id})
    }))
   // }
  },[outputFormats])

  React.useEffect(() => {
    if (!isPropsUpdate){
      const delayDebounceFn = setTimeout(() => {
          axios.put(`${config.digitalAssistantApi}/engine-api/intentions/${props.intention.id}/`,{
            title: intentionName,
          }).then((response) => {
          console.log(response)
          props.editList(response.data.title)
          })
        }, 1500)
      return () => clearTimeout(delayDebounceFn)
    }
  }, [intentionName]);

  const handleSelectionProps = (rowData) => {
    return {
      disabled:
        checked && checked.length >= 1 && !rowData.tableData.checked ? true : false
    }
  }

  const handleSelectChange = (rows) => {
    setChecked(rows)
  }

  const handleSelectFormats = (rows) => {
    setSlectedFormats(rows)
  }

  const changeIntentionName = (event) => {
    setIsPropsUpdate(false)
    setIntentionName(event.target.value)
  }

  const deleteIntention = () => {
    axios.delete(`${config.digitalAssistantApi}/engine-api/intentions/${props.intention.id}/`).then((response) =>{
      console.log(response)
      window.location.reload()
    })
  }

  return(
    <Container className={classes.module} maxWidth="lg">
      <Grid container spacing={2}>
        <Grid item xs={12} sm={4}>
          <Card>
            <CardContent>
              <FormControl>
                <TextField id="intention-name" label="Intention Name" value={intentionName} variant="outlined" onChange={changeIntentionName}/>
              </FormControl>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <MaterialTable
            title="Samples"
            columns={[{ title: 'Content', field: 'content' }]}
            options={{
              search: false,
              selection: true,
              showSelectAllCheckbox: false,
              showTextRowsSelected: false,
              selectionProps: handleSelectionProps,
              maxBodyHeight: 400,
              minBodyHeight: 400,
            }}
            data={samplesData}
            onSelectionChange={ (rows) => handleSelectChange(rows)}
            editable={{
              onRowAdd: (newData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  resolve();
                  axios.post(`${config.digitalAssistantApi}/engine-api/samples/`,{
                    content: newData.content,
                    intention: props.intention.id
                  }).then((response) =>{
                    console.log(response)
                    setSamples((prevState)=>{
                      const data = [...prevState]
                      data.push(response.data)
                      return data
                    })
                  })
                }, 600);
              }),
              
              onRowUpdate: (newData, oldData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  resolve();
                  if (oldData){
                    axios.put(`${config.digitalAssistantApi}/engine-api/samples/${oldData.id}/`,{
                      content: newData.content,
                      intention: props.intention.id
                    }).then((response) => {
                        
                        setSamples((prevState) => {
                        const data = [...prevState];
                        data[data.findIndex(item => item.id == oldData.id)] = response.data;
                        return data
                      });
                    })
                  }
                }, 600);
              }),
                            
              onRowDelete: (oldData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  resolve();
                  axios.delete(`${config.digitalAssistantApi}/engine-api/samples/${oldData.id}/`
                  ).then((response) =>{
                    console.log(response)
                    setSamples((prevState) => {
                      const data = [...prevState];
                      data.splice(data.findIndex(item => item.id == oldData.id), 1);
                      return data;
                    });
                  })
                }, 600);
              }),
            }}
          />
        </Grid>
        <Grid item xs={4}>
          <MaterialTable
            title={checked[0] ? <h3> Formats for {checked[0].content} </h3>: <h3>Output Formats</h3>}
            columns={[{ title: 'Format', field: 'output_format' }]}
            options={{
              search: false,
              selection: true,
              showSelectAllCheckbox: false,
              showTextRowsSelected: false,
              maxBodyHeight: 400,
              minBodyHeight: 400,
              
            }}
            onSelectionChange= {(rows) => handleSelectFormats(rows)}
            data={formatsData}
            editable={{
              onRowAdd: checked[0] ? (newData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  resolve();
                  axios.post(`${config.digitalAssistantApi}/engine-api/outputformats/`,{
                    output_format: newData.output_format,
                    sample: checked[0].id
                    }).then((response) =>{
                      console.log(response)
                      setOutputFormats((prevState)=>{
                        const data = [...prevState]
                        data.push(response.data)
                        return data
                     })
                    })
                  });
                }, 600) : null,
              
              onRowUpdate: (newData, oldData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  resolve();
                  if (oldData){
                    axios.put(`${config.digitalAssistantApi}/engine-api/outputformats/${oldData.id}/`,{
                      output_format: newData.output_format,
                      sample: checked[0].id
                    }).then((response) => {
                        setOutputFormats((prevState) => {
                          const data = [...prevState];
                          data[data.findIndex(item => item.id == oldData.id)] = response.data;
                          return data
                      });
                    })
                  }
                }, 600);
              }),
                            
              onRowDelete: (oldData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  resolve();
                  axios.delete(`${config.digitalAssistantApi}/engine-api/outputformats/${oldData.id}/`
                  ).then((response) =>{
                    console.log(response)
                    setOutputFormats((prevState) => {
                      const data = [...prevState];
                      data.splice(data.findIndex(item => item.id == oldData.id), 1);
                      return data;
                    })
                  });
                }, 600);
              }),
            }}
          />
        </Grid>
        <Grid item xs={4}>
          <MaterialTable
            title={'Dynamic Variables '}
            columns={[{title: 'Name', field: 'name'}]}
            options={{
              search: false,
              showSelectAllCheckbox: true,
              showTextRowsSelected: true,
              maxBodyHeight: 400,
              minBodyHeight: 400,
            }}
            data={intentionVariables.map((item) =>{
                    return({name: item.name, id: item.id})
                  })}
            actions={[
              {
                icon: EjectIcon,
                tooltip: 'add variable to checked formats',
                onClick: (event ,rowData) =>{
                  if (selectedFormats.length){
                      selectedFormats.forEach((format) =>{
                        axios.put(`${config.digitalAssistantApi}/engine-api/outputformats/${format.id}/`,{
                          output_format: format.output_format + '{' + rowData.name +'}',
                        }).then((response) => {
                          setOutputFormats((prevState) => {
                          const data = [...prevState];
                          data[data.findIndex(item => item.id == format.id)] = response.data;
                          return data
                        });
                        })
                      })
                  }
                }
              }
            ]}
            editable={{
              onRowAdd: (newData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  resolve();
                  axios.post(`${config.digitalAssistantApi}/engine-api/variables/`,{
                    name: newData.name,
                    intention: props.intention.id
                  }).then((response) =>{
                    console.log(response)
                    setIntentionVariables((prevState)=>{
                      const data = [...prevState]
                      data.push(response.data)
                      return data
                    })
                  })
                }, 600);
              }),
              
              onRowUpdate: (newData, oldData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  resolve();
                  if (oldData){
                    axios.put(`${config.digitalAssistantApi}/engine-api/variables/${oldData.id}/`,{
                      name: newData.name,
                      intention: props.intention.id
                    }).then((response) => {
                        setIntentionVariables((prevState) => {
                          console.log(oldData)
                        const data = [...prevState];
                        data[data.findIndex(item => item.id == oldData.id)] = response.data;
                        return data
                      });
                    })
                  }
                }, 600);
              }),
                            
              onRowDelete: (oldData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  resolve();
                  axios.delete(`${config.digitalAssistantApi}/engine-api/variables/${oldData.id}/`
                  ).then((response) =>{
                    console.log(response)
                    setIntentionVariables((prevState) => {
                      const data = [...prevState];
                      data.splice(data.findIndex(item => item.id == oldData.id), 1);
                      return data;
                    });
                  })
                }, 600);
              }),
            }}
          />
        </Grid>
        <Grid container spacing={4} alignItems="flex-start" justify="flex-end">
          <Grid item>
            <Button 
              color="secondary" 
              variant="contained" 
              className={classes.submitButtons}
              onClick={() => deleteIntention()}
            >
              Delete
            </Button> 
          </Grid>
        </Grid>
      </Grid>
    </Container>
  )
}



function Intentions(props){
  const classes = useStyles();
  const [intentions, setIntentions] = React.useState([])
  const [selectedIntention, setSelectedIntention] = React.useState({})
  const [change, setChnaged] = React.useState(true)

  React.useEffect(() => {
    axios.get(`${config.digitalAssistantApi}/engine-api/modules/${props.match.params.moduleId}/`).then((response) =>{
      setIntentions(response.data.module_intentions)
      if (props.location.intention){  
        setSelectedIntention(response.data.module_intentions.find(x =>x.id === props.location.intention ))
      }
    })
  },[])

  React.useEffect(() => {
    console.log(selectedIntention)
  },[selectedIntention])

  const handleToggle = (value) => {
    if (selectedIntention.id === value.id) {
      setSelectedIntention({})
    } else {
      setSelectedIntention(value)
    }
  };
  
  const handleCreateIntention = () => {
    axios.post(`${config.digitalAssistantApi}/engine-api/intentions/`,{
      title: 'New Intention',
      module: props.match.params.moduleId
    }).then((response) => {
      console.log(response)
      intentions.push(response.data)
      setSelectedIntention(response.data)

    })
  }
  
  const handleNameEdited = (name) => {
    intentions[intentions.findIndex(m => m.id === selectedIntention.id)].title = name
    setChnaged(!change)
  }
    

  return (
    <Container  maxWidth="lg" className={classes.paper}>
      <Grid container spacing={2}>
        <Grid item>
          {props.location.bot ? 
            
            <Button
              variant="outlined"
              startIcon={<ArrowBackIcon />}
              component={Link}
              to={{pathname: '/bots', bot: props.location.bot}} 
            >
              Go back
            </Button>
          :
            <Button
              variant="outlined"
              startIcon={<ArrowBackIcon />}
              component={Link}
              to={{pathname: '/modules', module: props.match.params.moduleId}} 
            >
              Go back
            </Button>
          }

        </Grid>
        <Grid item>
          <Typography className={classes.selectEmpty} component="h1" variant="h4">
            Intentions
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={4}>
        <Grid item xs={12} sm={6}>
          <Card className={classes.card}>
            <List subheader={<ListSubheader>Select an Intention</ListSubheader>} className={classes.list}>
              <Divider classes={{root: classes.divider}}light/>
              {intentions.map((item)=> {
                return(
                  <div key={item.id}>              
                    <ListItem
                      button
                      selected={item.id === selectedIntention.id ? true : false}
                      onClick={() => handleToggle(item)}
                    >
                      <ListItemText id="intention-title" primary={item.title} />
                    </ListItem>
                    <Divider classes={{root: classes.divider}}light/>
                  </div>
                )
              })}
            </List>
            <Button 
              variant="contained" 
              color="primary" 
              className={classes.createNew}
              onClick={() => handleCreateIntention()}
            >
              Or create one
            </Button>
          </Card>
        </Grid>
      </Grid>
      <Intention intention={selectedIntention} module={props.match.params.moduleId} editList={handleNameEdited}/>
    </Container>
  )
}

export default Intentions