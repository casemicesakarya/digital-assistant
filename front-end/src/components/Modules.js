import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import axios from 'axios'
import { Container, Typography, Button, Grid, List, ListItem, 
          ListItemText, ListSubheader, FormControl, MenuItem,
          InputLabel,  Select, Divider, TextField, Card, CardContent,
          ListItemIcon, Checkbox,} from '@material-ui/core';
import useStyles from './Styles.js'
const config = require('../config.js')


function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a, b) {
  return a.filter((value) => b.indexOf(value) !== -1);
}

function Module(props) {

  if (props.module.id === undefined){
    return null
  }
  const classes = useStyles();
  const [moduleTitle, setModuleTitle] = React.useState('')
  const [moduleDescription, setModuleDescriptiotn] = React.useState('')
  const [parentModule, setParentModule] = React.useState('')
  const [isPropsUpdate, setIsPropsUpdate] = React.useState(true)
  const [checked, setChecked] = React.useState([])
  const [left, setLeft] = React.useState([])
  const [right, setRight] = React.useState([])
  const leftChecked = intersection(checked, left)
  const rightChecked = intersection(checked, right)
  
  React.useEffect(()=> {
    setIsPropsUpdate(true)
    axios.get(`${config.digitalAssistantApi}/engine-api/modules/${props.module.id}/`)
      .then((response)=> {
        setModuleTitle(response.data.title)
        setModuleDescriptiotn(response.data.description)
        setParentModule(response.data.parent_module)
      })
  }, [props.module])

  React.useEffect(() => {
    if (!isPropsUpdate){
      const delayDebounceFn = setTimeout(() => {
          axios.put(`${config.digitalAssistantApi}/engine-api/modules/${props.module.id}/`,{
            title: moduleTitle,
            description: moduleDescription,
            parent_module: parentModule,
          }).then((response) => {
            console.log(response)
            props.editList(response.data.title)
          })
        }, 1500)
      return () => clearTimeout(delayDebounceFn)
    }
  },[moduleTitle,moduleDescription,parentModule])

  const handleParentSelect = (event) => {
    setParentModule(event.target.value)
  }

  const changeModuleTitle = (event) => {
    setIsPropsUpdate(false)
    setModuleTitle(event.target.value)
  }

  const changeModuleDescription = (event) => {
    setIsPropsUpdate(false)
    setModuleDescriptiotn(event.target.value)
  }

  const deleteModule = () => {
    axios.delete(`${config.digitalAssistantApi}/engine-api/modules/${props.module.id}/`).then((response) =>{
      console.log(response)
      window.location.reload()
    })
  }
    
  return (
    <Container key={props.module} className={classes.module} maxWidth="lg">
      <Card>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <FormControl>
                <TextField 
                  id="module-title" 
                  label="Module Title" 
                  variant="outlined" 
                  value={moduleTitle} 
                  onChange={changeModuleTitle}
                />
              </FormControl>
            </Grid>
            <Grid  item xs={12} sm={6}>
              <FormControl className={classes.formControl} >
                <TextField 
                  id="module-title" 
                  label="Module Description" 
                  variant="outlined"
                  multiline
                  rows={4}
                  value={moduleDescription} 
                  onChange={changeModuleDescription}
                />
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Button 
                component={Link}  
                to={{pathname: `/module-intentions/${props.module.id}`}} 
                size="large" 
                color="primary" 
                variant="contained"
                className={classes.intentionButton}
              >
                Intentions
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl className={classes.formControl}>
                <InputLabel variant="standard" id="parent-module-label">
                  Parent Module
                </InputLabel>
                <Select
                  labelId="parent-module"
                  inputProps={{
                      name: 'parent-module',
                      id: 'parent-module',
                    }}
                  value={parentModule}
                  onChange={handleParentSelect}
                >
                  <MenuItem value={null}><em>None</em></MenuItem>
                  {props.allModules.map((item) =>{
                    return (
                      <MenuItem key={item.id} value={item.id}>{item.title}</MenuItem>
                    )
                  })}
                </Select>
              </FormControl>
            </Grid>
          </Grid>
          <Grid container spacing={4} alignItems="flex-start" justify="flex-end">
            <Grid item>
              <Button 
                color="secondary" 
                variant="contained" 
                className={classes.submitButtons} 
                onClick={() => deleteModule()}
              >
                DELETE
              </Button> 
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Container>
  )
}


function Modules(props) {
  const classes = useStyles();
  const [modules, setModules] = React.useState([])
  const [selectedModule, setSelectedModule] = React.useState({})
  const [allModules, setAllModules] = React.useState([])
  const [change, setChnaged] = React.useState(true)

  React.useEffect(()=> {
    axios.get(`${config.digitalAssistantApi}/engine-api/modules/`).then((response) =>{
      setModules(response.data)
      if (props.location.module){
        setAllModules(response.data.filter(x => x.id != props.location.module))
        setSelectedModule(response.data.find(x => x.id == props.location.module))
      }
    })
  },[])

  const handleToggle = (value) => {
    if (selectedModule.id == value.id) {
      setSelectedModule({})
    } else {
      setAllModules(modules.filter(x => x.id != value))
      setSelectedModule(value)
    }
  };


  const handleCreateModel =() => {
    axios.post(`${config.digitalAssistantApi}/engine-api/modules/`,{
      title: 'New Module',
    }).then((response) => {
      console.log(response)
      setSelectedModule(response.data)
      modules.push(response.data)
      setAllModules(modules)
    })
  }

  const handleNameEdited = (name) => {
    modules[modules.findIndex(m => m.id === selectedModule.id)].title = name
    setChnaged(!change)
  }
    
  return (
    <Container className={classes.paper} maxWidth="lg">
      <Typography component="h1" variant="h4">
        Modules
      </Typography>
      <Grid container spacing={4}>
          <Grid item xs={12} sm={4}>
            <Card className={classes.card}>
              <List  subheader={<ListSubheader>Select A Module</ListSubheader>} className={classes.list}>
                <Divider classes={{root: classes.divider}}light/>
                {modules.map((item)=> {
                  return(
                    <div key={item.id}>
                      <ListItem 
                        button
                        selected={item.id == selectedModule.id ? true : false}
                        onClick={() => handleToggle(item)}
                      >
                        <ListItemText id="module-label" primary={item.title} />
                      </ListItem>
                      <Divider classes={{root: classes.divider}}light/>
                    </div>
                  )
                })}
              </List>
              <Button 
                variant="contained" 
                color="primary" 
                className={classes.createNew}
                onClick={() => handleCreateModel()}
              > 
                Or create one
              </Button>
            </Card>
          </Grid>
          <Grid item xs={12} sm={8}>
            <Module module={selectedModule} allModules={allModules} editList={handleNameEdited} />
          </Grid>
      </Grid>
    </Container>
  )
}       

export default Modules