import React  from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Link } from 'react-router-dom'
import { Container, Typography, Button, Grid, List, ListItem, 
          ListItemText, ListSubheader, FormControl,
          Divider, TextField, Card, CardContent,
          ListItemIcon, Checkbox, IconButton, ListItemSecondaryAction, Table,
          TableBody, TableCell, TableContainer, TableHead, TableRow,
          Paper, Toolbar } from '@material-ui/core';
import EjectIcon from '@material-ui/icons/Eject';
import MaterialTable from 'material-table';
import Cookie from 'universal-cookie'
import useStyles from './Styles.js'
const config = require('../config.js')
const cookie = new Cookie()

const columns = [
  {id: 'ratio', label:'Ratio', minWidth:100},
  {id: 'intent1', label: 'Intent-1', minWidth:130},
  {id: 'sample1', label: 'Sample-1', minWidth:170},
  {id: 'intent2', label: 'Intent-2', minWidth:130},
  {id: 'sample2', label: 'Sample-2', minWidth:170},

]

function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a, b) {
  return a.filter((value) => b.indexOf(value) !== -1);
}

function Bot(props) {
  if (props.bot.id === undefined){
    return null
  }
  
  const classes = useStyles();
  const [isPropsUpdate, setIsPropsUpdate] = React.useState(true)
  const [isTriggerChange, setIsTriggerChange] = React.useState(true)
  const [botName, setBotName] = React.useState('')
  const [botIsActive, setBotIsActive] = React.useState()
  const [botModules, setBotModules] = React.useState([])
  const [botTriggers, setBotTriggers] = React.useState([])
  const [similarity, setSimilarity] = React.useState([])
  const [similarityDate, setSimilarityDate] = React.useState(new Date())
  const [checkedModules, setCheckedModules] = React.useState([])
  const [selectedModule, setSelectedModule] = React.useState({})
  const [leftModules, setLeftModules] = React.useState([])
  const [rightModules, setRightModules] = React.useState([])
  const [botModuleVariables,setBotModuleVariables] = React.useState([])
  const leftCheckedModules = intersection(checkedModules, leftModules)
  const rightCheckedModules = intersection(checkedModules, rightModules)

  const [checkedTriggers, setCheckedTriggers] = React.useState([])
  const [selectedTrigger, setSelectedTrigger] = React.useState([])
  const [leftTriggers, setLeftTriggers] = React.useState([])
  const [rightTriggers, setRightTriggers] = React.useState([])
  const [botTriggerMessage, setBotTriggerMessage] = React.useState('')
  const [botTriggerMessageId, setBotTriggerMessageId] = React.useState('')
  const leftCheckedTriggers = intersection(checkedTriggers, leftTriggers)
  const rightCheckedTriggers = intersection(checkedTriggers, rightTriggers)

  React.useEffect(()=>{

  },[])

  React.useEffect(()=> {
    setIsPropsUpdate(true)
    setIsTriggerChange(true)
    axios.get(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/`)
    .then((response) => {
      setBotName(response.data.title)
      setBotModules(response.data.modules)
      setSelectedModule({})
      setSelectedTrigger({})
      setBotModuleVariables([])
      setBotTriggerMessage('') 
      setBotIsActive(response.data.is_active)
      setRightModules(props.modules.filter( item => response.data.modules.includes(item.id)))
      setLeftModules(props.modules.filter( item => !response.data.modules.includes(item.id)))
      setRightTriggers(props.triggers.filter( item => response.data.triggers.includes(item.id)))
      setLeftTriggers(props.triggers.filter( item => !response.data.triggers.includes(item.id)))
    })
    axios.get(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/get-similarity-check/`)
    .then((response)=>{
      console.log(response)
      setSimilarity(response.data.similarity)
      setSimilarityDate(new Date(response.data.similarity_date))
    })
  },[props.bot])

  React.useEffect(() => {
    if (!isPropsUpdate){
      axios.post(`${config.digitalAssistantApi}/bots-api/bot-value/get-variables-by-module/`,{
        module: selectedModule.id,
        bot: props.bot.id
      }).then((response) => {
        console.log(response)
        setBotModuleVariables(response.data)
      })
    }
  },[selectedModule])

  React.useEffect(() =>{
    if (!isPropsUpdate){
      axios.post(`${config.digitalAssistantApi}/bots-api/trigger-messages/get-message/`,{
        trigger: selectedTrigger.id,
        bot: props.bot.id
      }).then((response) => {
        setBotTriggerMessage(response.data.message)
        setBotTriggerMessageId(response.data.id)
      })
    }
  },[selectedTrigger])

  React.useEffect(() => {
    if (!isPropsUpdate){
      const delayDebounceFn = setTimeout(() => {
          axios.put(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/`,{
            title: botName,
            description: props.bot.description,
            modules: botModules,
            triggers: botTriggers,
            isActive: botIsActive
          }).then((response) => {
            console.log(response)
            props.editList(response.data.title)
          })
        }, 1000)
      return () => clearTimeout(delayDebounceFn)
    }
  },[botName,botModules,botTriggers,botIsActive])

  React.useEffect(() => {
    if (!isPropsUpdate && !isTriggerChange){
      const delayDebounceFn = setTimeout(() => {
        axios.put(`${config.digitalAssistantApi}/bots-api/trigger-messages/${botTriggerMessageId}/`,{
          trigger: selectedTrigger.id,
          message: botTriggerMessage,
          bot: props.bot.id,
        }).then((response) => {
            console.log(response)
          })
        }, 1000)
      return () => clearTimeout(delayDebounceFn)
    }
  },[botTriggerMessage])

  React.useEffect(() => {
    setBotModules(rightModules.map(item => item.id))
  },[rightModules])

  React.useEffect(() => {
    setBotTriggers(rightTriggers.map(item => item.id))
  },[rightTriggers])

  const changeBotName = (event) => {
    setIsPropsUpdate(false)
    setBotName(event.target.value)
  }

  const handleTriggerMessageChange = (event) => {
    setIsPropsUpdate(false)
    setIsTriggerChange(false)
    setBotTriggerMessage(event.target.value)
  }

  const toggleActivitiy = () => {
    axios.get(`${config.digitalAssistantApi}/core/${botIsActive ? 'stop-bot' :'start-bot'}/${props.bot.uuid}`).then((reponse) =>{
      console.log(reponse)
    })
    setBotIsActive(!botIsActive)
  }

  const buildBot = () => {
    axios.get(`${config.digitalAssistantApi}/core/build-bot/${props.bot.uuid}`).then((response)=>{
      console.log(response)
    })
  }

  const buildTriggers = () => {
    axios.get(`${config.digitalAssistantApi}/core/build-bot-triggers/${props.bot.uuid}`).then((response)=>{
      console.log(response)
    })
  }

  const checkSimilarity = () => {
    axios.get(`${config.digitalAssistantApi}/core/check-similarity/${props.bot.uuid}`).then((response)=>{
      console.log(response)
    })
  }
  
  const deleteBot = () => {
    axios.delete(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/`).then((response) =>{
      console.log(response)
      window.location.reload()
    })
  }

  const handleToggleModules = (value) => {
    const currentIndex = checkedModules.indexOf(value);
    const newChecked = [...checkedModules];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setCheckedModules(newChecked);
  };
  
  const handleToggleTriggers = (value) => {
    const currentIndex = checkedTriggers.indexOf(value);
    const newChecked = [...checkedTriggers];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setCheckedTriggers(newChecked);
  };

  const handleModuleSelect = (value) => {
    setIsPropsUpdate(false)
    if (selectedModule.id === value) {
      setSelectedModule({})
    } else {
      setSelectedModule(value)
    }
  }

  const handleTriggerSelect = (value) => {
    setIsPropsUpdate(false)
    setIsTriggerChange(true)
    if (selectedTrigger.id === value) {
      setSelectedTrigger({})
    } else {
      setSelectedTrigger(value)
    }
  }

  const handleCheckedRightModules = () => {
    axios.post(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/add-module/`,{
      modules: leftCheckedModules.map(m => m.id)
    }).then((response) =>{
      console.log(response)
    })
    setIsPropsUpdate(false)
    setRightModules(rightModules.concat(leftCheckedModules));
    setLeftModules(not(leftModules, leftCheckedModules));
    setCheckedModules(not(checkedModules, leftCheckedModules));
  };

  const handleCheckedRightTriggers = () => {
    axios.post(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/add-trigger/`,{
      triggers: leftCheckedTriggers.map(m => m.id)
    }).then((response) =>{
      console.log(response)
    })
    setIsPropsUpdate(false)
    setRightTriggers(rightTriggers.concat(leftCheckedTriggers));
    setLeftTriggers(not(leftTriggers, leftCheckedTriggers));
    setCheckedTriggers(not(checkedTriggers, leftCheckedTriggers));
  };

  const handleAllRightModules = () => {
    axios.post(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/add-module/`,{
      modules: rightCheckedModules.map(m => m.id)
    }).then((response) =>{
      console.log(response)
    })
    setIsPropsUpdate(false)
    setRightModules(rightModules.concat(leftModules));
    setLeftModules([]);
  };

  const handleAllRightTriggers = () => {
    axios.post(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/add-trigger/`,{
      triggers: rightCheckedTriggers.map(m => m.id)
    }).then((response) =>{
      console.log(response)
    })
    setIsPropsUpdate(false)
    setRightTriggers(rightTriggers.concat(leftTriggers));
    setLeftTriggers([]);
  };

  const handleCheckedLeftModules = () => {
    axios.post(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/remove-module/`,{
      modules: rightCheckedModules.map(m => m.id)
    }).then((response) =>{
      console.log(response)
    })
    setIsPropsUpdate(false)
    setBotModuleVariables([])
    setSelectedModule({})
    setLeftModules(leftModules.concat(rightCheckedModules));
    setRightModules(not(rightModules, rightCheckedModules));
    setCheckedModules(not(checkedModules, rightCheckedModules));
  };

  const handleCheckedLeftTriggers = () => {
    axios.post(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/remove-trigger/`,{
      triggers: rightCheckedTriggers.map(m => m.id)
    }).then((response) =>{
      console.log(response)
    })
    setIsPropsUpdate(false)
    setLeftTriggers(leftTriggers.concat(rightCheckedTriggers));
    setRightTriggers(not(rightTriggers, rightCheckedTriggers));
    setCheckedTriggers(not(checkedTriggers, rightCheckedTriggers));
  };

  const handleAllLeftModules = () => {
    setIsPropsUpdate(false)
    axios.post(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/remove-module/`,{
      modules: rightCheckedModules.map(m => m.id)
    }).then((response) =>{
      console.log(response)
    })
    setLeftModules(leftModules.concat(rightModules));
    setRightModules([]);
  };

  const handleAllLeftTriggers = () => {
    setIsPropsUpdate(false)
    axios.post(`${config.digitalAssistantApi}/bots-api/bots/${props.bot.id}/remove-trigger/`,{
      triggers: rightCheckedTriggers.map(m => m.id)
    }).then((response) =>{
      console.log(response)
    })
    setLeftTriggers(leftTriggers.concat(rightTriggers));
    setRightTriggers([]);
  };





  const customList = (items, title, secondAction, modules) => (
    <Card className={classes.transferPaper}>
      <List dense component="div" role="list" subheader={<ListSubheader>{title}</ListSubheader>} className={classes.smallList}>
      <Divider classes={{root: classes.divider}}light/> 
        {items.map((item) => {
          const labelId = `transfer-list-item-${item}-label`;
          return (
            <div key={item}>
              <ListItem role="listitem" button onClick={modules ? () => handleToggleModules(item): () => handleToggleTriggers(item)}>
                <ListItemIcon>
                  <Checkbox
                    checked={modules ? checkedModules.indexOf(item) !== -1 : checkedTriggers.indexOf(item) !== -1}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ 'aria-labelledby': labelId }}
                  />
                </ListItemIcon>
                <ListItemText id={labelId} primary={item.title} />
                { secondAction ? 
                  <ListItemSecondaryAction>
                    <IconButton 
                      color="primary" 
                      aria-label="upload picture" 
                      component="span"
                      onClick={modules ? ()=> handleModuleSelect(item) : () => handleTriggerSelect(item)}
                    >
                      <EjectIcon/>
                    </IconButton>
                  </ListItemSecondaryAction> 
                  : 
                  null
                }
              </ListItem>
              <Divider classes={{root: classes.divider}}light/>
            </div>
          );
        })}
        <ListItem />
      </List>
    </Card>
  );

  return (
    <Container key={props.module} className={classes.module} maxWidth="lg">
      <Card>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <FormControl>
                <TextField id="bot-name" label="Bot Name" variant="outlined" value={botName} onChange={changeBotName}/>
              </FormControl>
            </Grid>
          </Grid>
          <Card variant="outlined" className={classes.botCard}>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item sm={12}>
                  <Typography color="textPrimary" variant="h5" align="center">Bot Triggers</Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center" alignItems="center" >
                <Grid container spacing={2} sm={8} justify="center" alignItems="center" className={classes.transferRoot}>
                  <Grid item>{customList(leftTriggers, 'Triggers',false,false)}</Grid>
                  <Grid item>
                    <Grid container direction="column" alignItems="center">
                      {/* <Button
                        variant="outlined"
                        size="small"
                        className={classes.button}
                        onClick={handleAllRightTriggers}
                        disabled={leftTriggers.length === 0}
                        aria-label="move all right"
                      >
                        ≫
                      </Button> */}
                      <Button
                        variant="outlined"
                        size="small"
                        className={classes.transferButton}
                        onClick={handleCheckedRightTriggers}
                        disabled={leftCheckedTriggers.length === 0}
                        aria-label="move selected right"
                      >
                        &gt;
                      </Button>
                      <Button
                        variant="outlined"
                        size="small"
                        className={classes.transferButton}
                        onClick={handleCheckedLeftTriggers}
                        disabled={rightCheckedTriggers.length === 0}
                        aria-label="move selected left"
                      >
                        &lt;
                      </Button>
                      {/* <Button
                        variant="outlined"
                        size="small"
                        className={classes.button}
                        onClick={handleAllLeftTriggers}
                        disabled={rightTriggers.length === 0}
                        aria-label="move all left"
                      >
                      ≪
                    </Button> */}
                    </Grid>
                  </Grid>
                  <Grid item>{customList(rightTriggers, 'Bot Triggers',true,false)}</Grid>
                </Grid>
                <Grid item xs={12} sm={4} className={classes.marge}>
                  <FormControl className={classes.formControl} >
                    <TextField 
                      id="trigger-message" 
                      label={selectedTrigger.id ? "Trigger Message for " + selectedTrigger.title : "Trigger Message "} 
                      variant="outlined"
                      multiline
                      rows={4}
                      disabled={selectedTrigger.id == null}
                      value={botTriggerMessage} 
                      onChange={handleTriggerMessageChange}
                    />
                  </FormControl>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
          <Card>
            <CardContent className={classes.botCard}>
              <Grid container spacing={2}>
                <Grid item sm={12}>
                  <Typography color="textPrimary" variant="h5" align="center">Bot Modules</Typography>
                </Grid>
              </Grid>
              <Grid container spacing={2}>
                <Grid container spacing={2} justify="center" alignItems="center" className={classes.transferRoot}>
                  <Grid item>{customList(leftModules,'Modules',false,true)}</Grid>
                  <Grid item>
                    <Grid container direction="column" alignItems="center">
                      {/* <Button
                        variant="outlined"
                        size="small"
                        className={classes.button}
                        onClick={handleAllRightModules}
                        disabled={leftModules.length === 0}
                        aria-label="move all right"
                      >
                        ≫
                      </Button> */}
                      <Button
                        variant="outlined"
                        size="small"
                        className={classes.transferButton}
                        onClick={handleCheckedRightModules}
                        disabled={leftCheckedModules.length === 0}
                        aria-label="move selected right"
                      >
                        &gt;
                      </Button>
                      <Button
                        variant="outlined"
                        size="small"
                        className={classes.transferButton}
                        onClick={handleCheckedLeftModules}
                        disabled={rightCheckedModules.length === 0}
                        aria-label="move selected left"
                      >
                        &lt;
                      </Button>
                      {/* <Button
                        variant="outlined"
                        size="small"
                        className={classes.button}
                        onClick={handleAllLeftModules}
                        disabled={rightModules.length === 0}
                        aria-label="move all left"
                      >
                      ≪
                    </Button> */}
                    </Grid>
                  </Grid>
                  <Grid item>{customList(rightModules, 'Bot Modules', true,true)}</Grid>
                </Grid>
              </Grid>
              <Grid container spacing={2} justify="center" alignItems="center" className={classes.marge}>
                  <Grid item xs={12} sm={12}>
                    <MaterialTable
                      title={selectedModule.id ? 'Bot Variables for module ' + selectedModule.title : 'Bot Variables' }
                      columns={[{ title: 'Variable', field: 'name',editable: 'never'},{title: 'Intention', field: 'intention',editable:'never'},{title: 'value', field: 'value'}]}
                      options={{
                        search: false,
                        selection: false,
                        showSelectAllCheckbox: false,
                        showTextRowsSelected: false,
                        maxBodyHeight: 400,
                        minBodyHeight: 400,
                      }}
                      localization={{
                        body: {
                          emptyDataSourceMessage: "Select a module to display it's variables"
                        }
                      }}
                      data={botModuleVariables}
                      editable={{
                        onRowUpdate: (newData, oldData) =>
                        new Promise((resolve) => {
                          setTimeout(() => {
                            resolve();
                            if (oldData){
                              axios.put(`${config.digitalAssistantApi}/bots-api/bot-value/${oldData.id}/`,{
                                value: newData.value,
                                variable: newData.variable,
                                bot: props.bot.id,
                              }).then((response) => {
                                  setBotModuleVariables((prevState) => {
                                  const data = [...prevState];
                                  data[data.findIndex(item => item.id == oldData.id)] = response.data;
                                  return data
                                });
                              })
                            }
                          }, 600);
                        }),           
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <Paper className={classes.paperTable}>
                      <Toolbar>
                        <Typography className={classes.tableTitle}  variant="h6" id="tableTitle" component="div">
                          {similarity.length ==! 0 ? "Similarity, Check Date: " + similarityDate.toLocaleString() : "No similarity check was made for this bot"}
                        </Typography>
                      </Toolbar>
                      <TableContainer className={classes.tableContainer}>
                        <Table stickyHeader aria-label="sticky table">
                          <TableHead>
                            <TableRow>
                              {columns.map((column) => (
                                <TableCell
                                  key={column.id}
                                  style={{ minWidth: column.minWidth }}
                                >
                                  {column.label}
                                </TableCell>
                              ))}
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {similarity.map((row) => {
                              return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                  {columns.map((column) => {
                                    const value = row[column.id];
                                    console.log(column)
                                    return (
                                      column.id.includes("intent") ? 
                                        <TableCell key={column.id} align={column.align}>
                                          <Typography 
                                          component={Link} 
                                          to={ column.id ==="intent1" ? 
                                            { pathname: `/module-intentions/${row.intent1_module}`, intention: row.intent1_id, bot: props.bot.id }
                                            :
                                            { pathname: `/module-intentions/${row.intent2_module}`, intention: row.intent2_id, bot: props.bot.id }
                                          } 
                                          color="textPrimary"  
                                          style={{ textDecoration: 'none' }}
                                          >
                                            {value}
                                          </Typography>
                                        </TableCell>
                                      :
                                        <TableCell key={column.id} align={column.align}>
                                          <Typography color="textPrimary">
                                            {value}
                                          </Typography>
                                        </TableCell>
                                    );
                                  })}
                                </TableRow>
                              );
                            })}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </Paper>
                  </Grid>
              </Grid>
              <Grid container spacing={4} alignItems="flex-start" justify="flex-end">
                <Grid item>
                  <Button 
                    color="primary" 
                    variant="contained" 
                    className={classes.submitButtons} 
                    onClick={() => checkSimilarity()}
                  >
                    CHECK SIMILARITY 
                  </Button> 
                </Grid>
                <Grid item>
                  <Button 
                    color="primary" 
                    variant="contained" 
                    className={classes.submitButtons} 
                    onClick={() => buildBot()}
                  >
                    BUILD
                  </Button> 
                </Grid>
                <Grid item>
                  <Button 
                    color="primary" 
                    variant="contained" 
                    className={classes.submitButtons} 
                    onClick={() => buildTriggers()}
                  >
                    BUILD TRIGGERS
                  </Button> 
                </Grid>
                <Grid item>
                  <Button
                    color={botIsActive ? 'secondary' : 'primary'}
                    variant="contained"
                    className={classes.submitButtons}
                    onClick={() => toggleActivitiy()}
                  >
                  {botIsActive ? 'STOP' : 'START'}
                  </Button>
                </Grid>
                <Grid item>
                  <Button 
                    color="secondary" 
                    variant="contained" 
                    className={classes.submitButtons} 
                    onClick={() => deleteBot()}
                  >
                    DELETE
                  </Button> 
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </CardContent>
      </Card>
    </Container>
  )
}


function Bots(props) {
  const classes = useStyles();
  const [bots, setBots] = React.useState([])
  const [botsInfo, setBotsInfo] = React.useState([])
  const [modules, setModules] = React.useState([])
  const [triggers, setTriggers] = React.useState([])
  const [selectedBot, setSelectedBot] = React.useState({})
  const [change, setChnaged] = React.useState(true)

  
  React.useEffect(() => {
    axios.get(`${config.digitalAssistantApi}/bots-api/bots/`).then((response) =>{
      setBots(response.data)
      if (props.location.bot){
        console.log("yes")
        setSelectedBot(response.data.find(x =>x.id == props.location.bot ))
      }
    })
    axios.get(`${config.digitalAssistantApi}/engine-api/modules/`).then((response) =>{
      setModules(response.data)
    })
    axios.get(`${config.digitalAssistantApi}/engine-api/triggers/`).then((response) =>{
      setTriggers(response.data)
    })
  },[])

  // React.useEffect(() => {
  //   var pro = new Promise((resolve, reject)=>{
  //     bots.forEach((element,index,array) => {
  //       axios.get(`${config.digitalAssistantApi}/core/bot-status/${element.uuid}`).then((res) =>{
  //         element['status'] = res.data.response
  //         if (index + 1 === array.length) resolve()
  //       })
  //     });
  //   })
  //   pro.then(() =>{
  //     setBotsInfo(bots)
  //   })
  // },[bots])

  const handleToggle = (value) => {
    if (selectedBot.id == value){
      setSelectedBot({})
    } else { 
      setSelectedBot(bots.find(x => x.id === value))
    }
  }

  const handleCreateBot = () => {
    axios.post(`${config.digitalAssistantApi}/bots-api/bots/`,{
      title: 'New Bot',
    }).then((response) =>{
      console.log(response)
      bots.push(response.data)
      setSelectedBot(response.data)
      
    })
  }
  
  const handleNameEdited = (name) => {
    bots[bots.findIndex(m => m.id === selectedBot.id)].title = name
    setChnaged(!change)
  }

  return (
        <Container key={props.module} className={classes.module} maxWidth="lg">
          <Typography component="h1" variant="h4">
            Bots
          </Typography>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={4}>
                <Card className={classes.card}>
                  <List  subheader={<ListSubheader>Select Bot</ListSubheader>} className={classes.list}>
                    <Divider classes={{root: classes.divider}}light/>
                    {bots.map((item)=> {
                      return(
                        <div key={item.id}>
                          <ListItem 
                            button
                            selected={item.id === selectedBot.id}
                            onClick={() => handleToggle(item.id)}
                          >
                            <ListItemText id='bot-label' primary={item.title} />
                            <ListItemSecondaryAction>
                              <ListItemText id='bot-status' primary={item.status} />
                            </ListItemSecondaryAction> 
                          </ListItem>
                          <Divider classes={{root: classes.divider}}light/>
                        </div>
                      )
                    })}
                  </List>
                  <Button 
                    variant="contained" 
                    color="primary" 
                    className={classes.createNew}
                    onClick={() => handleCreateBot()}
                  > 
                    Or create one
                  </Button>
                </Card>
                <Grid item xs={12} sm={12}>
                  <Button
                    variant="contained" 
                    color="primary"
                    className={classes.testButton}
                    component={Link}
                    to={cookie.get('token')=== undefined ? {pathname: `/login`} : {pathname: `/test-platform`}} 
                  >
                  Test Platform
                  </Button>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={12}>
                <Bot bot={selectedBot} editList={handleNameEdited} modules={modules} triggers={triggers}/>
              </Grid>
            </Grid>
          </Container>
    )
}

export default Bots