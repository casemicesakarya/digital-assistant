import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios'
import moment from 'moment'
import { Container, Typography, Button, Grid, List, ListItem, 
          ListItemText, ListSubheader, FormControl, MenuItem,
          InputLabel,  Select, Divider, TextField, Card, CardContent } from '@material-ui/core';
import { KeyboardDatePicker, MuiPickersUtilsProvider, KeyboardTimePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import useStyles from './Styles.js'
const config = require('../config.js')


function isNumeric(n) {
  return (!isNaN(parseInt(n)) && isFinite(n)) || n === '';
}

function Trigger (props) {
  
  if (props.trigger.id === undefined){
    return null
  }

  const classes = useStyles();
  const [triggerTitle, setTriggerTitle] = React.useState('')
  const [triggerType, setTriggerType] = React.useState('')
  const [keywords, setKeywords] = React.useState('')
  const [interval, setInterval] = React.useState(0)
  const [intervalVariance, setIntervalVariance] = React.useState(0)
  const [eventDate, setEventDate] = React.useState(Date.now())
  const [isPropsUpdate, setIsPropsUpdate] = React.useState(true)


  
  React.useEffect(() => {
    setIsPropsUpdate(true)
    axios.get(`${config.digitalAssistantApi}/engine-api/triggers/${props.trigger.id}/`)
      .then((response)=> {
        setTriggerType(response.data.trigger_type)
        setTriggerTitle(response.data.title)
        setKeywords(response.data.keywords)
        setInterval(response.data.interval)
        setIntervalVariance(response.data.interval_variance)
        setEventDate(response.data.event_date)
        console.log(response.data.event_date)
      })
  },[props.trigger])

  React.useEffect(() => {
    if (!isPropsUpdate){
      const delayDebounceFn = setTimeout(() => {
        axios.put(`${config.digitalAssistantApi}/engine-api/triggers/${props.trigger.id}/`,{
          title: triggerTitle,
          trigger_type: triggerType,
        }).then((response)=> {
          console.log(response)
          props.editList(response.data.title)
        })
      } ,1500)
      return () => clearTimeout(delayDebounceFn)
    }
  },[triggerTitle,triggerType])

  React.useEffect(() => {
    if (!isPropsUpdate){
      const delayDebounceFn = setTimeout(() => {
        axios.put(`${config.digitalAssistantApi}/engine-api/triggers/${props.trigger.id}/`,{
          title: triggerTitle,
          trigger_type: triggerType,
          event_date: eventDate ? moment(eventDate).format() : null,
          interval: interval,
          interval_variance: intervalVariance,
          keywords: keywords
        }).then((response)=> {
          console.log(response)
        })
      } ,1500)
      return () => clearTimeout(delayDebounceFn)
    }

  },[keywords,interval,intervalVariance,eventDate])

  const changeTriggerTitle = (event) => {
    setIsPropsUpdate(false)
    setTriggerTitle(event.target.value)
  }

  const changekeywords = (event) => {
    setIsPropsUpdate(false)
    setInterval(null)
    setIntervalVariance(null)
    setEventDate(null)
    setKeywords(event.target.value)
  }

  const changeEventDate = (date) => {
    setIsPropsUpdate(false)
    setInterval(null)
    setIntervalVariance(null)
    setKeywords(null)
    setEventDate(date)
  }

  const changeInterval = (event) => {
      setIsPropsUpdate(false)
      if (isNumeric(event.target.value)){
        setKeywords(null)
        setEventDate(null)
        setInterval(event.target.value)
      }
      return null
  }

  const changeIntervalVariance = (event) => {
    setIsPropsUpdate(false)
    if (isNumeric(event.target.value)){
      setKeywords(null)
      setEventDate(null)
      setIntervalVariance(event.target.value)
    }
    return null
  }
  
  const handleTypeSelection = (event) => {
    setIsPropsUpdate(false)
    setTriggerType(event.target.value)
  };

  const deleteTrigger = () => {
    axios.delete(`${config.digitalAssistantApi}/engine-api/triggers/${props.trigger.id}/`).then((response) =>{
      console.log(response)
      window.location.reload()
    })
  }

  return (
    <Container key={props.trigger} className={classes.module} maxWidth="lg">
      <Card>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <FormControl>
                <TextField 
                  id="trigger-title" 
                  label="Trigger Ttile" 
                  variant="outlined" 
                  value={triggerTitle} 
                  onChange={changeTriggerTitle}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl className={classes.formControl} >
                <InputLabel variant="standard" id="trigger-type">
                  Trigger Type
                </InputLabel>
                <Select
                  labelId="trigger-type"
                  inputProps={{
                    name: 'triggerType',
                  }}
                  value={triggerType}
                  onChange={handleTypeSelection}
                  className={classes.selectEmpty}
                >
                  <MenuItem value={'C'}>Chronic</MenuItem>
                  <MenuItem value={'E'}>Event</MenuItem>
                  <MenuItem value={'M'}>Manual</MenuItem>
                  <MenuItem value={'I'}>Idle</MenuItem>
                  <MenuItem value={'W'}>Keywords</MenuItem>
                  <MenuItem value={'D'}>Data</MenuItem>
                  <MenuItem value={'A'}>Inactive</MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
          {triggerType === 'W' ?           
            <Card>
              <CardContent>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <Typography>Trigger Properties</Typography>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormControl className={classes.formControl}>
                        <TextField 
                          id="keywords" 
                          label="Keywords" 
                          variant="outlined" 
                          multiline
                          rows={4}
                          helperText="keywords separated by a comma, ex:'sport,football'"
                          value={keywords} 
                          onChange={changekeywords}/>
                    </FormControl>
                  </Grid>
                </Grid>
              </CardContent>
            </Card> 
            : null 
          }
          {triggerType === 'E' ?           
            <Card>
              <CardContent>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <Typography>Trigger Properties</Typography>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          margin="normal"
                          format="MM/dd/yyyy"
                          id="event-date" 
                          label="Event Date" 
                          value={eventDate} 
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                          onChange={changeEventDate}
                        />
                        <KeyboardTimePicker
                          margin="normal"
                          id="event-time"
                          label="Event Time"
                          value={eventDate}
                          onChange={changeEventDate}
                          KeyboardButtonProps={{
                            'aria-label': 'change time',
                          }}
                        />
                    </MuiPickersUtilsProvider>
                  </Grid>
                </Grid>
              </CardContent>
            </Card> 
            : null 
          }
          {triggerType === 'C' || triggerType === 'I' ?           
            <Card>
              <CardContent>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <Typography>Trigger Properties</Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <FormControl className={classes.formControl}>
                        <TextField 
                          id="interval" 
                          label="Interval" 
                          variant="outlined" 
                          value={interval}
                          helperText="Interval in minutes"
                          onChange={changeInterval}
                        />
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormControl className={classes.formControl}>  
                        <TextField 
                          id="interval-variance" 
                          label="Interval Variance" 
                          variant="outlined" 
                          value={intervalVariance}
                          helperText="Interval Variance in minutes"
                          onChange={changeIntervalVariance}
                        />
                    </FormControl>
                  </Grid>
                </Grid>
              </CardContent>
            </Card> 
            : null 
          }
          <Grid container spacing={4} alignItems="flex-start" justify="flex-end">
            <Grid item>
              <Button 
                color="secondary" 
                variant="contained" 
                className={classes.submitButtons} 
                onClick={() => deleteTrigger()}
              >
                DELETE
              </Button> 
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Container>
  )
}


function Triggers (props) {
  const classes = useStyles();
  const [triggers, setTriggers] = React.useState([])
  const [selectedTrigger, setSelectedTrigger] = React.useState({})
  const [change, setChnaged] = React.useState(true)

  React.useEffect(()=> {
    axios.get(`${config.digitalAssistantApi}/engine-api/triggers/`).then((response) =>{
      setTriggers(response.data)
    })
  },[])

  const handleNameEdited = (name) => {
    triggers[triggers.findIndex(m => m.id === selectedTrigger.id)].title = name
    setChnaged(!change)
  }

  const handleToggle = (value) => {
    if (selectedTrigger.id == value.id) {
      setSelectedTrigger({})
    } else {
      setSelectedTrigger(value)
    }
  };

  const handleCreateTrigger = () => {
    axios.post(`${config.digitalAssistantApi}/engine-api/triggers/`,{
      title: 'New Trigger',
      trigger_type: 'A',
    }).then((response) => {
      console.log(response)
      triggers.push(response.data)
      setSelectedTrigger(response.data)
    })
  }

  return(
    <Container className={classes.paper} maxWidth="lg">
      <Typography component="h1" variant="h4">
        Triggers
      </Typography>
      <Grid container spacing={4}>
          <Grid item xs={12} sm={5}>
            <Card className={classes.card}>
              <List  subheader={<ListSubheader>Triggers List</ListSubheader>} className={classes.list}>
                <Divider classes={{root: classes.divider}}light/>
                {triggers.map((item)=> {
                  return(
                    <div key={item.id}>
                      <ListItem 
                        button
                        selected={item.id === selectedTrigger.id ? true : false}
                        onClick={() => handleToggle(item)}
                      >
                        <ListItemText id="trigger-label" primary={item.title} />
                      </ListItem>
                      <Divider classes={{root: classes.divider}}light/>
                    </div>
                  )
                })}
              </List>
              <Button 
                variant="contained" 
                color="primary" 
                className={classes.createNew}
                onClick={() => handleCreateTrigger()}
              > 
                Or create one
              </Button>
            </Card>
          </Grid>
          <Grid item xs={12} sm={7}>
            <Trigger trigger={selectedTrigger} editList={handleNameEdited}/>
          </Grid>
      </Grid>
    </Container>
    )
}


export default Triggers