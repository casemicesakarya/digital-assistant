import { makeStyles, useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  
  transferRoot: {
    // margin: 'auto',
    // borderWidth: 1,
    // borderColor: '#e0e0e0',
    // borderStyle: 'solid',
    // marginTop: theme.spacing(1),
    minHeight: 300,
  },
  paper:{
    marginTop: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    transform: 'scale(1)',
    
  },
  marge: {
    marginTop: theme.spacing(1.5),
  },
  transferPaper: {
    width: 260,
    height: 230,
    overflow: 'auto',
    display: 'flex',
  },
  transferButton: {
    margin: theme.spacing(0.5, 0),
  },
  list: {
    width: '100%',
    maxWidth: 360,
    minHeight: 200,
    backgroundColor: theme.palette.background.paper,
    overflow: 'auto',
    maxHeight: 450,
  },
  smallList: {
    width: '100%',
    overflow: 'auto',
    backgroundColor: theme.palette.background.paper,
    borderColor: '#e0e0e0',
    borderStyle: 'solid',
  },
  createNew: {
    width: '100%',
    maxWidth: 360,
    
  },
  testButton: {
    width: '100%',
    maxWidth: 360,
    marginTop: theme.spacing(1.5),
    
  },
  card: {
    width: '100%',
    maxWidth: 360,
  },
  module: {
    marginTop: theme.spacing(3)
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,

  },
  divider: {
    backgroundColor: '#C8C8C8',
  },
  intentionButton: {
    marginTop: theme.spacing(2),
    minWidth: 220,
  },
  submitButtons: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(3),
    maxHeight: 50
  },
  PlatformContainer: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
    height: "87vh",
  },
  channelList: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
  },
  chat: {
    display: "flex",
    flex: 3,
    flexDirection: "column",
    borderWidth: "1px",
    borderColor: "#ccc",
    borderRightStyle: "solid",
    borderLeftStyle: "solid",
  },
  scrollChat: {
    overflow: 'auto',
  },
  settings: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
  },
  botCard: {
    marginTop: theme.spacing(1.5),
    borderWidth: 1,
    borderColor: '#e0e0e0',
    borderStyle: 'solid',
  },
  tableContainer: {
    maxHeight: 500,
  },
  paperTable: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  tableTitle: {
    flex: '1 1 100%',
  },
}))

export default useStyles