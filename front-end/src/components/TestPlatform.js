import React, {useCallback}  from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import useStyles from './Styles.js'
import {Avatar, List, ListItem, ListItemAvatar, 
        ListItemText, DialogTitle, Dialog, Typography,
        AppBar, Toolbar} from "@material-ui/core"
import { GiftedChat } from "react-web-gifted-chat";
import Cookie from 'universal-cookie'
const config = require('../config.js')
const cookie = new Cookie()


function TestPlatform(props){

  if (!cookie.get('token')){
    props.history.push('/login')
  }
  const classes = useStyles();
  const [messages, setMessages] = React.useState([])
  const [bots,setBots] = React.useState([])
  const [selectedBot, setSelectedBot] = React.useState({})

  const onSend = (newMsg: Imessage[]) => {
    axios.post(`${config.digitalAssistantApi}/send-receive/`,{
      text: newMsg[0].text,
      user: cookie.get("user_id"),
      bot: selectedBot.id
      },{
        headers: {
            Authorization:  "Token " + cookie.get("token"),
        }            
      }
      ).then((response) =>{
        console.log(response)
          var botMessage = [{
              user: {
              id: '-1',
              name: 'Bot',
              avatar: 'https://placeimg.com/140/140/any',
            },
            id: Math.random(),
            text: response.data.text,
            createdAt: new Date(),
          }]
          botMessage.push(newMsg[0])
          setMessages(previousMessages => GiftedChat.append(previousMessages, botMessage))
        }).catch((error)=>{
          console.log(error)
          console.log("Got error while trying to save bot message to database ")
        })
  }

  const handleToggle = (value) => {
    if (selectedBot.id == value){
      setSelectedBot({})
    } else { 
      setSelectedBot(bots.find(x => x.id === value))
    }
  }

  React.useEffect(() => {
    axios.get(`${config.digitalAssistantApi}/bots-api/bots/`).then((response) =>{
      setBots(response.data)
    }) 

  },[])

  React.useEffect(() =>{
    if (Object.keys(selectedBot).length){
      axios.post(`${config.digitalAssistantApi}/bots-api/chat-histroy/get-messages/`,{
          bot: selectedBot.id,
          user: cookie.get("user_id"),
        },{
          headers: {
            Authorization:  "Token " + cookie.get("token"),
          }     
        }).then((response) => {
          console.log(response)
          var msgs = []
          var pro = new Promise((resolve, reject) =>{
            response.data.forEach((message,index,array)=>{
              var m = {
                user: {
                  id: message.is_from_bot === true ? '-1' : message.user + '',
                  name: message.is_from_bot === true ? '-1' : message.user + '',
                  avatar: 'https://placeimg.com/140/140/any',
                },
                id: Math.random(),
                text: message.text,
                createdAt:message.create_date,

              }
              msgs.push(m)
              if (index + 1 === array.length) resolve()
            })
          })
          pro.then(()=>{
            setMessages(previousMessages => GiftedChat.append(previousMessages, msgs))
          })
        })
    }
  },[selectedBot])


  return(
    <div className={classes.PlatformContainer}>
      <div className={classes.channelList}>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="h6" color="inherit">
              Bots
            </Typography>
          </Toolbar>
        </AppBar>
        <List>
          { bots.map((item) =>{
            return(
              <div key={item.id}>
                <ListItem 
                  selected={item.id === selectedBot.id ? true : false}
                  button
                  onClick={() => handleToggle(item.id)}
                >
                  <ListItemAvatar>
                    <Avatar>{item.title[0]}</Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={item.title} />
                </ListItem>
              </div>
          )})
          }
        </List>
      </div>
      <div className={classes.chat}>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="h6" color="inherit">
              {selectedBot.id ? selectedBot.title : 'Select a bot'}
            </Typography>
          </Toolbar>
        </AppBar>
        {selectedBot.id ?
            <GiftedChat
              messages={messages}
              onSend={(messages)=> onSend(messages)}
              user={{
                id: cookie.get("user_id"),
                name: 'me'
              }}
            /> 
          : null
        }
      </div>
    </div>
  )
}

export default TestPlatform

